import java.util.Scanner;

public class bs4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int num1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int num2 = scanner.nextInt();
        int resultado = num1 * num2;
        System.out.printf("El producto de %d y %d es: %d\n", num1, num2, resultado);
    }
}
