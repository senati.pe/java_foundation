import java.util.Scanner;

public class bs19 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int num = scanner.nextInt();
        int suma = 0;

        for (int i = 1; i <= num; i++) {
            suma += i;
        }
        System.out.println("La suma de todos los números enteros desde 1 hasta " + num + " es: " + suma);
    }
}
