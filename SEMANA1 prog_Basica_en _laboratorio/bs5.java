import java.util.Scanner;

public class bs5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int num1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int num2 = scanner.nextInt();
        int diferencia = num1 - num2;
        System.out.printf("La diferencia entre %d y %d es: %d\n", num1, num2, diferencia);
    }
}
