import java.util.Scanner;

public class bs6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int num1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int num2 = scanner.nextInt();
        int cociente = num1 / num2;
        int resto = num1 % num2;
        System.out.printf("El cociente de %d y %d es: %d\n", num1, num2, cociente);
        System.out.printf("El resto de %d y %d es: %d\n", num1, num2, resto);
    }
}
