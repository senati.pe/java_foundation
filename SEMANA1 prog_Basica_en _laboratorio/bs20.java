import java.util.Scanner;

public class bs20 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int num1 = scanner.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int num2 = scanner.nextInt();

        int a = num1;
        int b = num2;
        while (b != 0) {
            int temp = b;
            b = a % b;
            a = temp;
        }
        int mcd = a;

        int mcm = (num1 * num2) / mcd;

        System.out.println("El Máximo Común Divisor de " + num1 + " y " + num2 + " es: " + mcd);
        System.out.println("El Mínimo Común Múltiplo de " + num1 + " y " + num2 + " es: " + mcm);
    }
}
