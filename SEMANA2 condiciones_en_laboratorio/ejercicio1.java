import java.util.Scanner;

public class ejercicio6 {

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese un número: ");
        int limite = sc.nextInt();
        
        int n1 = 0, n2 = 1, suma;
        System.out.print(n1 + " " + n2 + " ");
        
        while (n2 < limite) {
            suma = n1 + n2;
            System.out.print(suma + " ");
            n1 = n2;
            n2 = suma;
        }
        
        sc.close();
    }

}
