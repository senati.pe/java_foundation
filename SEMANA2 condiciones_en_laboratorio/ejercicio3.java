import java.util.Scanner;

public class ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese un número decimal: ");
        int decimal = sc.nextInt();

        System.out.print("Ingrese la base a la que desea convertir (2-9): ");
        int base = sc.nextInt();

        if (base < 2 || base > 9) {
            System.out.println("La base debe estar entre 2 y 9.");
        } else {
            String resultado = "";

            while (decimal > 0) {
                int residuo = decimal % base;
                resultado = residuo + resultado;
                decimal /= base;
            }

            System.out.println("El equivalente en base " + base + " es: " + resultado);
        }

        sc.close();
    }
}
