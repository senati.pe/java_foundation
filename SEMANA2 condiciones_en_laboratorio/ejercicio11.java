public class ejercicio11 {
    public static void main(String[] args) {
        int altura = 7;
        int anchura = altura * 2 - 1;
        int medio = anchura / 2;
        
        for (int i = 0; i < altura; i++) {
            for (int j = 0; j < anchura; j++) {
                if (j >= medio - i && j <= medio + i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
        
        for (int i = altura - 2; i >= 0; i--) {
            for (int j = 0; j < anchura; j++) {
                if (j >= medio - i && j <= medio + i) {
                    System.out.print("*");
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }
}
