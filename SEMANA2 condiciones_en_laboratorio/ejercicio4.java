import java.util.Scanner;

public class ejercicio4 {
  public static void main(String[] args) {
    Scanner input = new Scanner(System.in);

    System.out.print("Introduce un número entero n: ");
    int n = input.nextInt();

    int suma = 0;
    int num = 1;
    int cantidad = 0;

    while (cantidad < n) {
      if (esPerfecto(num)) {
        suma += num;
        cantidad++;
      }
      num++;
    }

    System.out.println("La suma de los primeros " + n + " números perfectos es: " + suma);
  }

  public static boolean esPerfecto(int n) {
    int sum = 0;

    for (int i = 1; i < n; i++) {
      if (n % i == 0) {
        sum += i;
      }
    }

    return sum == n;
  }
}
