public class ejercicio5 {
    public static void main(String[] args) {
        int max = 100;
        System.out.println("Los números primos entre 2 y " + max + " son:");
        for(int i=2; i<=max; i++) {
            if(esPrimo(i)) {
                System.out.println(i);
            }
        }
    }
    
    public static boolean esPrimo(int n) {
        if (n<=1) return false;
        for(int i=2; i<=Math.sqrt(n); i++) {
            if(n%i == 0) {
                return false;
            }
        }
        return true;
    }
}
