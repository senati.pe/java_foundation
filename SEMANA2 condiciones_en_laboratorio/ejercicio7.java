import java.util.Scanner;

public class ejercicio7 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, i = 1, suma = 0;
        System.out.print("Introduce el número de pares que quieres sumar: ");
        n = sc.nextInt();

        do {
            if (i % 2 == 0) {
                suma += i;
                n--;
            }
            i++;
        } while (n > 0);

        System.out.println("La suma de los " + (i-1)/2 + " primeros pares es " + suma);
    }

}
