import java.util.Scanner;

public class ejercicio2 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese un número N para sumar los N números impares: ");
        int n = input.nextInt();

        int suma = 0;
        int contador = 0;
        int num = 1;

        while (contador < n) {
            if (num % 2 != 0) {
                suma += num;
                contador++;
            }
            num++;
        }

        System.out.println("La suma de los " + n + " primeros números impares es: " + suma);
    }
}
