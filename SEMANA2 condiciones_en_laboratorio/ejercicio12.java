import java.util.Scanner;

public class ejercicio12 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce la cantidad de números: ");
        int cantidad = scanner.nextInt();
        
        int[] numeros = new int[cantidad];
        for(int i = 0; i < cantidad; i++) {
            System.out.print("Introduce el número " + (i+1) + ": ");
            numeros[i] = scanner.nextInt();
        }
        
        int mcm = numeros[0];
        for(int i = 1; i < numeros.length; i++) {
            mcm = calcularMcm(mcm, numeros[i]);
        }
        System.out.println("El Mínimo Común Múltiplo de los números es: " + mcm);
    }
    
    private static int calcularMcm(int a, int b) {
        return (a*b) / calcularMcd(a, b);
    }
    
    private static int calcularMcd(int a, int b) {
        int resto;
        while(b != 0) {
            resto = a % b;
            a = b;
            b = resto;
        }
        return a;
    }
}
