import java.util.Scanner;

public class ejercicio14 {

    public static void main(String[] args) {
        
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingresa un número para calcular su factorial: ");
        int num = sc.nextInt();
        int resultado = 1;
        
        while(num > 0) {
            resultado *= num;
            num--;
        }
        
        System.out.println("El factorial del número ingresado es: " + resultado);
    }

}
