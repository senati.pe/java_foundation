import java.util.Scanner;

public class ejercicio9 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int num, sum = 0, i = 1;
        System.out.print("Ingrese un número: ");
        num = input.nextInt();
        do {
            if (num % i == 0) {
                sum = sum + i;
            }
            i++;
        } while (i <= num / 2);
        if (sum == num) {
            System.out.println(num + " es un número perfecto");
        } else {
            System.out.println(num + " no es un número perfecto");
        }
    }
}
