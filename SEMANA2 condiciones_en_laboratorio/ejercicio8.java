import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números a verificar: ");
        int n = sc.nextInt();
        int[] numeros = new int[n];
        int maximoComunDivisor = 1;
        boolean continuar = true;
        int divisor = 2;
        int contador;

        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el número " + (i+1) + ": ");
            numeros[i] = sc.nextInt();
        }

        while (continuar) {
            contador = 0;
            for (int i = 0; i < n; i++) {
                if (numeros[i] % divisor == 0) {
                    numeros[i] /= divisor;
                    contador++;
                }
            }
            if (contador == n) {
                maximoComunDivisor *= divisor;
            } else {
                divisor++;
            }
            continuar = false;
            for (int i = 0; i < n; i++) {
                if (numeros[i] != 1) {
                    continuar = true;
                    break;
                }
            }
        }

        System.out.println("El Maximo Comun divisor es: " + maximoComunDivisor);
        sc.close();
    }
}
