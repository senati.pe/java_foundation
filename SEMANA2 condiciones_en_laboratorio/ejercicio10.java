import java.util.Random;
import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Random rand = new Random();

        int numGenerado = rand.nextInt(100) + 1; // Generamos un número aleatorio del 1 al 100
        int intentos = 10; // Número de intentos disponibles
        int numAdivinado;

        System.out.println("Adivina el número generado por la aplicación (entre 1 y 100):");

        do {
            System.out.println("Intentos restantes: " + intentos);
            numAdivinado = sc.nextInt();
            intentos--;

            if (numAdivinado < numGenerado) {
                System.out.println("El número a adivinar es mayor.");
            } else if (numAdivinado > numGenerado) {
                System.out.println("El número a adivinar es menor.");
            } else {
                System.out.println("¡Acertaste en " + (10 - intentos) + " intentos!");
                return; // Terminamos el programa
            }
        } while (intentos > 0);

        System.out.println("Agotaste los intentos, el número era " + numGenerado);
    }
}
