public class ejercicio15 {
    public static void main(String[] args) {
        int limit = 50;  // límite de la secuencia
        int num1 = 2, num2 = 1; // primeros dos números de Lucas
        int sum;

        System.out.print(num1 + " " + num2 + " ");

        while ((sum = num1 + num2) <= limit) {
            System.out.print(sum + " ");
            num1 = num2;
            num2 = sum;
        }
    }
}
