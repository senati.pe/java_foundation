import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        
        System.out.print("Ingrese la cantidad de productos: ");
        int n = input.nextInt();
        
        String[] productos = new String[n];
        
        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese el producto " + (i+1) + ": ");
            productos[i] = input.next();
        }
        
        System.out.println("\nLos productos ingresados son: ");
        for (int i = 0; i < n; i++) {
            System.out.println((i+1) + ". " + productos[i]);
        }
        
        input.close();
    }
}
