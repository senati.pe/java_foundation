import java.util.Scanner;

public class ejercicio17 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la longitud del lado del cuadrado: ");
        int longitud = sc.nextInt();

        for (int i = 1; i <= longitud; i++) {
            for (int j = 1; j <= longitud; j++) {
                boolean borde = (i == 1 || i == longitud || j == 1 || j == longitud);
                System.out.print(borde ? "* " : "  ");
            }
            System.out.println();
        }
    }
}
