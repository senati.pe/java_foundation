public class ejercicio3 {
    private String nombre;
    private int edad;
    private String paisNacimiento;
    private char genero;
    private String profesion;
    private boolean esEstudiante;

    public ejercicio3(String nombre, int edad, String paisNacimiento, char genero, String profesion,
            boolean esEstudiante) {
        this.nombre = nombre;
        this.edad = edad;
        this.paisNacimiento = paisNacimiento;
        this.genero = genero;
        this.profesion = profesion;
        this.esEstudiante = esEstudiante;
    }

    public String getNombre() {
        return nombre;}
    public void setNombre(String nombre) {
        this.nombre = nombre;}
    public int getEdad() {
        return edad;}
    public void setEdad(int edad) {
        this.edad = edad;}
    public String getPaisNacimiento() {
        return paisNacimiento;}
    public void setPaisNacimiento(String paisNacimiento) {
        this.paisNacimiento = paisNacimiento;}
    public char getGenero() {
        return genero;}
    public void setGenero(char genero) {
        this.genero = genero;}
    public String getProfesion() {
        return profesion;}
    public void setProfesion(String profesion) {
        this.profesion = profesion;}
    public boolean isEsEstudiante() {
        return esEstudiante;}
    public void setEsEstudiante(boolean esEstudiante) {
        this.esEstudiante = esEstudiante;}

    public static void main(String[] args) {
        ejercicio3 persona = new ejercicio3("Juan", 30, "México", 'H', "Ingeniero", false);
        System.out.println("Nombre: " + persona.getNombre());
        System.out.println("Edad: " + persona.getEdad());
        System.out.println("País de nacimiento: " + persona.getPaisNacimiento());
        System.out.println("Género: " + persona.getGenero());
        System.out.println("Profesión: " + persona.getProfesion());
        System.out.println("¿Es estudiante?: " + persona.isEsEstudiante());
    }
}