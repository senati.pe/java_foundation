public class ejercicio4 {
    private String nombre;
    private int edad;
    private String paisNacimiento;
    private char genero;
    private String profesion;
    private boolean esEstudiante;

    public ejercicio4(String nombre, int edad, String paisNacimiento, char genero, String profesion,
            boolean esEstudiante) {
        this.nombre = nombre;
        this.edad = edad;
        this.paisNacimiento = paisNacimiento;
        this.genero = genero;
        this.profesion = profesion;
        this.esEstudiante = esEstudiante;
    }

    public String getNombre() {
        return nombre;}
    public void setNombre(String nombre){
        this.nombre = nombre;}
    public int getEdad(){
        return edad;}
    public void setEdad(int edad) {
        this.edad = edad;}
    public String getPaisNacimiento() {
        return paisNacimiento;}
    public void setPaisNacimiento(String paisNacimiento){
        this.paisNacimiento = paisNacimiento;}
    public char getGenero() {
        return genero;}
    public void setGenero(char genero) {
        this.genero = genero;}
    public String getProfesion() {
        return profesion;}
    public void setProfesion(String profesion) {
        this.profesion = profesion;}
    public boolean isEsEstudiante() {
        return esEstudiante;}
    public void setEsEstudiante(boolean esEstudiante) {
        this.esEstudiante = esEstudiante;}
    public void imprimir() {
        System.out.println("Profesión: " + profesion);
        System.out.println("¿Es estudiante?: " + esEstudiante);
    }

    public static void main(String[] args) {
        ejercicio4 persona = new ejercicio4("Juan", 30, "México", 'H', "Ingeniero", false);
        persona.imprimir();
    }
}