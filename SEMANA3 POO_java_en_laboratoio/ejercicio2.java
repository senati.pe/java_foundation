public class ejercicio2 {
    private String nombre;
    private int edad;
    private String paisNacimiento;
    private char genero;

    public ejercicio2(String nombre, int edad, String paisNacimiento, char genero) {
        this.nombre = nombre;
        this.edad = edad;
        this.paisNacimiento = paisNacimiento;
        this.genero = genero;
    }

    public String getNombre() {
        return nombre;}
    public void setNombre(String nombre) {
        this.nombre = nombre;}
    public int getEdad() {
        return edad;}
    public void setEdad(int edad) {
        this.edad = edad;}
    public String getPaisNacimiento() {
        return paisNacimiento;}
    public void setPaisNacimiento(String paisNacimiento) {
        this.paisNacimiento = paisNacimiento;}
    public char getGenero() {
        return genero;}
    public void setGenero(char genero) {
        this.genero = genero;}

    c
        ejercicio2 persona = new ejercicio2("Juan", 30, "México", 'H');
        System.out.println("Nombre: " + persona.getNombre());
        System.out.println("Edad: " + persona.getEdad());
        System.out.println("País de nacimiento: " + persona.getPaisNacimiento());
        System.out.println("Género: " + persona.getGenero());
    }
}