import java.time.LocalDate;

public class ejercicio1 {
    private String nombre;
    private String apellido;
    private int dni;
    private int anioNacimiento;

    public ejercicio1(String nombre, String apellido, int dni, int anioNacimiento) {
        this.nombre = nombre;
        this.apellido = apellido;
        this.dni = dni;
        this.anioNacimiento = anioNacimiento;
    }

    public String getNombre() {
        return nombre;}
    public void setNombre(String nombre) {
        this.nombre = nombre;}
    public String getApellido() {
        return apellido; }
    public void setApellido(String apellido) {
        this.apellido = apellido;}
    public int getDni() {
        return dni;}
    public void setDni(int dni) {
        this.dni = dni;}
    public int getAnioNacimiento() {
        return anioNacimiento;}
    public void setAnioNacimiento(int anioNacimiento) {
        this.anioNacimiento = anioNacimiento;}
    public int getEdad() {
        int edad = LocalDate.now().getYear() - anioNacimiento;
        return edad;}

    public void imprimirDatos() {
        System.out.println("Nombre: " + nombre);
        System.out.println("Apellido: " + apellido);
        System.out.println("DNI: " + dni);
        System.out.println("Año de nacimiento: " + anioNacimiento);
        System.out.println("Edad: " + getEdad());
    }

    public static void main(String[] args) {
        ejercicio1 persona1 = new ejercicio1("Juan", "Pérez", 12345678, 1990);
        ejercicio1 persona2 = new ejercicio1("María", "González", 23456789, 1985);

        persona1.imprimirDatos();
        System.out.println();
        persona2.imprimirDatos();
    }
}