import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        int numero, centenas, decenas, unidades;
        
        System.out.print("Ingrese un número de tres cifras: ");
        numero = input.nextInt();
        
        centenas = numero / 100;
        decenas = (numero % 100) / 10;
        unidades = numero % 10;
        
        System.out.println("Centenas: " + centenas);
        System.out.println("Decenas: " + decenas);
        System.out.println("Unidades: " + unidades);
    }
}
