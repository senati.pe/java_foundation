import java.util.Scanner;

public class ejercicio19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n, num, count = 0;
        System.out.print("Introduce la cantidad de números a evaluar: ");
        n = sc.nextInt();
        for (int i = 1; i <= n; i++) {
            System.out.print("Introduce el número " + i + ": ");
            num = sc.nextInt();
            if (num % 10 == 2) {
                count++;
            }
        }
        System.out.println("La cantidad de números que acaban en 2 es: " + count);
    }
}
