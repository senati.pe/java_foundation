import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int N = sc.nextInt();
        System.out.print("Ingrese la cantidad de cifras a quitar: ");
        int m = sc.nextInt();

        // obtener la potencia de 10 correspondiente a m cifras
        int p = (int) Math.pow(10, m);

        // quitar las últimas m cifras de N
        N /= p;
        N *= p;

        System.out.println("El resultado es: " + N);
    }
}
