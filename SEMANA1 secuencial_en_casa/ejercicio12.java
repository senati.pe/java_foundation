import java.util.Scanner;

public class ejercicio12 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero de 5 cifras: ");
        int numero = sc.nextInt();
        System.out.print("Las cifras del número son: ");
        while (numero > 0) {
            int cifra = numero % 10;
            System.out.print(cifra + " ");
            numero = numero / 10;
        }
        sc.close();
    }
}
