import java.util.Scanner;

public class ejercicio7 {
  public static void main(String[] args) {
    double cateto1, cateto2, hipotenusa;

    Scanner input = new Scanner(System.in);

    System.out.println("Introduce la longitud del primer cateto: ");
    cateto1 = input.nextDouble();

    System.out.println("Introduce la longitud del segundo cateto: ");
    cateto2 = input.nextDouble();

    hipotenusa = Math.sqrt(Math.pow(cateto1, 2) + Math.pow(cateto2, 2));

    System.out.println("La longitud de la hipotenusa es: " + hipotenusa);
  }
}
