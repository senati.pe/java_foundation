import java.util.Scanner;

public class ejercicio13 {
  public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);

    System.out.print("Ingresa tu fecha de nacimiento (dia mes año): ");
    int day = scanner.nextInt();
    int month = scanner.nextInt();
    int year = scanner.nextInt();

    int sum = day + month + year;
    while (sum >= 10) {
      String digits = String.valueOf(sum);
      sum = 0;
      for (int i = 0; i < digits.length(); i++) {
        sum += Character.getNumericValue(digits.charAt(i));
      }
    }

    System.out.printf("Tu número de la suerte es: %d%n", sum);
  }
}
