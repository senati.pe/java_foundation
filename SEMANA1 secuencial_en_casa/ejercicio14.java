import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Ingrese el precio de costo del producto: ");
        double precioCosto = sc.nextDouble();
        
        System.out.print("Ingrese el porcentaje de ganancia que desea obtener (por ejemplo, 25 para un 25%): ");
        double porcentajeGanancia = sc.nextDouble();
        
        double ganancia = precioCosto * (porcentajeGanancia / 100);
        double precioVenta = precioCosto + ganancia;
        
        System.out.println("El precio final de venta del producto es: " + precioVenta);
    }
}
