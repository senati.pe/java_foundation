import java.util.Scanner;

public class ejercicio9 {

    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Ingrese la longitud del primer lado");
        double a = input.nextDouble();
        System.out.println("Ingrese la longitud del segundo lado");
        double b = input.nextDouble();
        System.out.println("Ingrese la longitud del tercer lado");
        double c = input.nextDouble();

        double p = (a + b + c) / 2;
        double area = Math.sqrt(p * (p - a) * (p - b) * (p - c));

        System.out.println("El area del triangulo es: " + area);
    }
}
