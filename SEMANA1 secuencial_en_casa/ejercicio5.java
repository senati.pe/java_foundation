import java.util.Scanner;

public class ejercicio5 {
   public static void main(String[] args) {
      double radio, length, area;
      Scanner in = new Scanner(System.in);  

      System.out.print("Introduzca el radio del círculo.: ");
      radio = in.nextDouble();  
      
      length = 2 * Math.PI * radio; 
      area = Math.PI * Math.pow(radio, 2); 

      System.out.println("Longitud del círculo = " + length);
      System.out.println("area del circulo = " + area);
   }
}
