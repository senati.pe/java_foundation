import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        char repeat;
        do {
            System.out.print("Ingrese la temperatura en grados Celsius: ");
            double celsius = input.nextDouble();
            double kelvin = celsius + 273.15;
            System.out.println("La temperatura en grados Kelvin es: " + kelvin);
            System.out.print("¿Desea repetir el proceso? (S/N): ");
            repeat = input.next().charAt(0);
        } while (repeat == 'S' || repeat == 's');
        System.out.println("Fin del programa.");
    }
}
