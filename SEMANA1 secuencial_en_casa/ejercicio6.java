import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la velocidad en Km/h: ");
        double velocidadKmH = sc.nextDouble();
        sc.close();
        double velocidadMS = velocidadKmH * 1000 / 3600;
        System.out.println("La velocidad en m/s es: " + velocidadMS);
    }
}
