import java.util.Scanner;

public class ejercicio11 {
   public static void main(String[] args) {
      Scanner scanner = new Scanner(System.in);
      System.out.print("Ingrese un número entero de 5 cifras:");
      int numero = scanner.nextInt();
      String numeroString = String.valueOf(numero);
      for (int i = 0; i < numeroString.length(); i++) {
         System.out.println(numeroString.charAt(i));
      }
   }
}
