import java.util.Arrays;

public class ejercicio5 {
    private String tipoFractal;
    private int nivelRecursion;

    public Fractal(String tipoFractal, int nivelRecursion) {
        this.tipoFractal = tipoFractal;
        this.nivelRecursion = nivelRecursion;
    }

    public void construir() {
        if (tipoFractal.equals("Koch")) {
            construirKoch();
        }  else if (tipoFractal.equals("Sierpinski")) {
            construirSierpinski();
        } else {
            System.out.println("Tipo de fractal no válido.");
        }
    }

    private void construirKoch() {
        if (nivelRecursion == 0) {
            dibujarLinea();
        } else {
            construirKoch(nivelRecursion - 1);
            girarIzquierda(60);
            construirKoch(nivelRecursion - 1);
            girarDerecha(120);
            construirKoch(nivelRecursion - 1);
            girarIzquierda(60);
            construirKoch(nivelRecursion - 1);
        }
    }

    private void construirKoch(int nivel) {
        if (nivel == 0) {
            dibujarLinea();
        }else {
            construirKoch(nivel - 1);
            girarIzquierda(60);
            construirKoch(nivel - 1);
            girarDerecha(120);
            construirKoch(nivel - 1);
            girarIzquierda(60);
            construirKoch(nivel - 1);
        }
    }

    private void construirSierpinski() {
        if (nivelRecursion == 0) {
            dibujarTriangulo();
        } else {
            construirSierpinski(nivelRecursion);
        }
    }

    private void construirSierpinski(int nivel) {
        if (nivel == 0) {
            dibujarTriangulo();
        }  else {
            construirSierpinski(nivel - 1);
            double longitudLado = obtenerLongitudLadoTriangulo() / Math.pow(2, nivel - 1);
            moverAdelante(longitudLado);
            construirSierpinski(nivel - 1);
            girarIzquierda(120);
            moverAdelante(longitudLado);
            girarIzquierda(120);
            construirSierpinski(nivel - 1);
            moverAdelante(longitudLado);
            girarIzquierda(120);
            moverAdelante(longitudLado);
            girarDerecha(120);
        }
    }

    private void dibujarLinea() {
           System.out.println("Dibujando línea.");
    }

    private void dibujarTriangulo() {
        System.out.println("Dibujando triángulo.");
    }

    private void moverAdelante(double distancia) {
           System.out.println("Moviendo adelante " + distancia + " unidades.");
    } 

    private void girarIzquierda(double angulo) {
        System.out.println("Girando " + angulo + " grados hacia la izquierda.");
    }

    private void girarDerecha(double angulo) {
        System.out.println("Girando " + angulo + " grados hacia
