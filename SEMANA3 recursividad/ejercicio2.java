public class ejercicio2 {
    public static void main(String[] args) {
        int[] numeros = {12, 24, 36, 48}; 
        int n = numeros.length;
        int mcd = mcd(numeros, n);
        System.out.println("El minimoComunD de los números es: " + mcd);
    }
    
    public static int mcd(int[] numeros, int n) {
        if (n == 1) {
            return numeros[0];
        } else {
            return mcdAux(numeros, n - 1, numeros[n-1]);
        }
    }
    
    public static int mcdAux(int[] numeros, int n, int mcdActual) {
        if (n == 0) {
            return mcdActual;
        } else {
            int resto = numeros[n-1] % mcdActual;
            int nuevoMCD = (resto == 0) ? mcdActual : mcdAux(numeros, n - 1, resto);
            return nuevoMCD;
        }
    }
}
