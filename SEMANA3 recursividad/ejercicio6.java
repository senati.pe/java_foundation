import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;

public class ejercicio6 {

    public static void main(String[] args) {
        final int N = 1000; 
        final int MIN = 0; 
        final int MAX = 300; 

        try {
            FileWriter writer = new FileWriter("datos_generados.txt");
            Random random = new Random();
            for (int i = 0; i < N; i++) {
                int dato = random.nextInt(MAX - MIN + 1) + MIN;
                writer.write(dato + "\n");
            }
            writer.close();
        } catch (IOException e) {
            System.out.println("Error al escribir archivo: " + e.getMessage());
            return;
        }

        int[] datos = new int[N];
        double suma = 0.0;
        double sumaCuadrados = 0.0;
        int maximo = Integer.MIN_VALUE;
        int minimo = Integer.MAX_VALUE;
        Map<Integer, Integer> frecuencias = new HashMap<>();
        try {
            Scanner scanner = new Scanner(new File("datos_generados.txt"));
            while (scanner.hasNextLine()) {
                int dato = Integer.parseInt(scanner.nextLine());
                datos[frecuencias.size()] = dato;
                suma += dato;
                sumaCuadrados += dato * dato;
                maximo = Math.max(maximo, dato);
                minimo = Math.min(minimo, dato);
                frecuencias.put(dato, frecuencias.getOrDefault(dato, 0) + 1);
            }
            scanner.close();
        } catch (IOException e) {
            System.out.println("Error al leer archivo: " + e.getMessage());
            return;
        }

        int moda = -1;
        int frecuenciaModa = -1;
        for (Map.Entry<Integer, Integer> entry : frecuencias.entrySet()) {
            if (entry.getValue() > frecuenciaModa) {
                moda = entry.getKey();
                frecuenciaModa = entry.getValue();
            }
        }

        double promedio = suma / N;
        double desviacion = Math.sqrt(sumaCuadrados / N - promedio * promedio);

        try {
            FileWriter writer = new FileWriter("resultados.txt");
            writer.write("Valor máximo: " + maximo + "\n");
            writer.write("Valor mínimo: " + minimo + "\n");
            writer.write("Promedio: " + promedio + "\n");
            writer.write("Moda: " + moda + "\n");
            writer.write("Desviación estándar: " + desviacion + "\n");
            writer.close();
        } catch (IOException e) {
            System.out.println("Error al escribir archivo: " + e.getMessage());
            return;
        }
    }

}
