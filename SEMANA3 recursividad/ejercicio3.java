public class ejercicio3 {
    
    public static void main(String[] args) {
        int[] nums = { 12, 24, 36, 48 };
        int result = mcm(nums);
        System.out.println("El mínimo común múltiplo de los números es " + respuesta);
    }

    public static int mcm(int[] nums) {
        if (nums.length == 2) {
             return mcm(nums[0], nums[1]);
        } else {
            int[] remainingNums = new int[nums.length - 1];
               System.arraycopy(nums, 1, remainingNums, 0, nums.length - 1);
            return mcm(nums[0], mcm(remainingNums));
        }
    }

    public static int mcm(int a, int b) {
        return (a * b) / mcd(a, b);
    }

    public static int mcd(int a, int b) {
        if (b == 0) {
            return a;
        } else {
            return mcd(b, a % b);
        }
    }
}
