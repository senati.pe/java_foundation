public class ejercicio4 {
    private String tipoFractal;
    private int nivelRecursion;

    public Fractal(String tipoFractal, int nivelRecursion) {
        this.tipoFractal = tipoFractal;
        this.nivelRecursion = nivelRecursion;
    }

    public void construir() {
        if (tipoFractal.equals("Koch")) {
            construirKoch();
        } else if (tipoFractal.equals("Sierpinski")) {
            construirSierpinski();
        } else {
            System.out.println("Tipo de fractal no válido.");
        }
    }

    private void construirKoch() {
        if (nivelRecursion == 0) {
            
        } else {
            construirKoch();
            construirKoch();
            construirKoch();
            construirKoch();
            construirKoch();
        }
    }

    private void construirSierpinski() {
        if (nivelRecursion == 0) {
        } else {
            construirSierpinski();
            construirSierpinski();
            construirSierpinski();
        }
    }
}
