public class ejercicio7 {
    public static void quickSort(int[] arreglo, int izquierda, int derecha) {
        if (izquierda < derecha) {
            int indiceParticion = particion(arreglo, izquierda, derecha);
            quickSort(arreglo, izquierda, indiceParticion - 1);
            quickSort(arreglo, indiceParticion + 1, derecha);
        }
    }

    private static int particion(int[] arreglo, int izquierda, int derecha) {
        int pivote = arreglo[derecha];
        int indiceParticion = izquierda;
        for (int i = izquierda; i < derecha; i++) {
            if (arreglo[i] <= pivote) {
                intercambiar(arreglo, i, indiceParticion);
                indiceParticion++;
            }
        }
        intercambiar(arreglo, indiceParticion, derecha);
        return indiceParticion;
    }

    private static void intercambiar(int[] arreglo, int i, int j) {
        int temp = arreglo[i];
        arreglo[i] = arreglo[j];
        arreglo[j] = temp;
    }

    public static void main(String[] args) {
        int[] arreglo = { 5, 3, 8, 4, 2 };
        System.out.println("Arreglo sin ordenar: " + Arrays.toString(arreglo));
        quickSort(arreglo, 0, arreglo.length - 1);
        System.out.println("Arreglo ordenado: " + Arrays.toString(arreglo));
    }
}