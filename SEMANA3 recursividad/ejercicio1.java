public class ejercicio1 {
    public static int fibonacci(int n) {
        if (n <= 1) {
             return n;
        } else {
            int a = 0;
            int b = 1;
            int c = 0;
              for (int i = 2; i <= n; i++) {
                c = a + b;
                a = b;
                b = c;
            }
            return c;
        }
    }

    public static void main(String[] args) {
        int n = 10;
        System.out.println("El " + n + "-ésimo término de la serie de Fibonacci es: " + fibonacci(n));
    }
}