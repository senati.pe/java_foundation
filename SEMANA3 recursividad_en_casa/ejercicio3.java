public class ejercicio3 {
    public static void main(String[] args) {
        int m = 2; // valor de m
        int n = 1; // valor de n
        int resultado = ackermann(m, n); // calcular el enésimo término
        System.out.println("El enésimo término de la secuencia de Ackermann para m = " + m + " y n = " + n + " es: " + resultado);
    }

    public static int ackermann(int m, int n) {
        if (m == 0) {
            return n + 1;
        } else if (m > 0 && n == 0) {
            return ackermann(m - 1, 1);
        } else {
            return ackermann(m - 1, ackermann(m, n - 1));
        }
    }
}
