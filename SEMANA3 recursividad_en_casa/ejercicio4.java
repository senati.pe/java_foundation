import java.util.Scanner;

public class ejercicio4 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Ingrese un número: ");
        int n = scanner.nextInt();
        scanner.close();
        
        for (int i = n; i >= 1; i--) {
            System.out.print(i + " ");
        }
    }
}
