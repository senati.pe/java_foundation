import java.util.Scanner;

public class ejercicio1 {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Introduce la base: ");
        int base = scanner.nextInt();
        System.out.print("Introduce el exponente: ");
        int exponente = scanner.nextInt();
        scanner.close();

        double resultado = Math.pow(base, exponente);
        System.out.println("El resultado de " + base + " elevado a " + exponente + " es: " + resultado);
    }
}
