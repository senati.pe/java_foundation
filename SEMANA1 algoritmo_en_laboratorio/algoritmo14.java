import java.util.Scanner;

public class algoritmo14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese las palabras separadas por espacios: ");
        String palabrasStr = sc.nextLine();
        String[] palabrasArr = palabrasStr.split(" ");
        int countPalindromos = 0;
        for (int i = 0; i < palabrasArr.length; i++) {
            String palabra = palabrasArr[i];
            if (esPalindromo(palabra)) {
                countPalindromos++;
            }
        }
        System.out.println("Hay " + countPalindromos + " palíndromos en la lista.");
        sc.close();
    }

    public static boolean esPalindromo(String palabra) {
        int inicio = 0;
        int fin = palabra.length() - 1;
        while (inicio < fin) {
            if (palabra.charAt(inicio) != palabra.charAt(fin)) {
                return false;
            }
            inicio++;
            fin--;
        }
        return true;
    }
}
