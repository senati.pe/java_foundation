import java.util.Scanner;

public class algoritmo2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero: ");
        int numero = sc.nextInt();
        if (numero > 0) {
            System.out.println(numero + " es un número positivo.");
        } else if (numero < 0) {
            System.out.println(numero + " es un número negativo.");
        } else {
            System.out.println(numero + " es cero.");
        }
        sc.close();
    }
}
