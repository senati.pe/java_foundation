import java.util.Scanner;

public class algoritmo15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese las palabras separadas por espacios: ");
        String palabrasStr = sc.nextLine();
        System.out.print("Ingrese la letra a buscar: ");
        char letra = sc.next().charAt(0);
        String[] palabrasArr = palabrasStr.split(" ");
        int countPalabrasConLetra = 0;
        for (int i = 0; i < palabrasArr.length; i++) {
            String palabra = palabrasArr[i];
            if (palabra.charAt(0) == letra) {
                countPalabrasConLetra++;
            }
        }
        System.out.println("Hay " + countPalabrasConLetra + " palabras que comienzan con la letra " + letra);
        sc.close();
    }
}
