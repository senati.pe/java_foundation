import java.util.Scanner;

public class algoritmo12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese los números separados por espacios: ");
        String numerosStr = sc.nextLine();
        String[] numerosArr = numerosStr.split(" ");
        int countNumerosPerfectos = 0;
        for (int i = 0; i < numerosArr.length; i++) {
            int num = Integer.parseInt(numerosArr[i]);
            if (esNumeroPerfecto(num)) {
                countNumerosPerfectos++;
            }
        }
        System.out.println("Hay " + countNumerosPerfectos + " números perfectos en la lista.");
        sc.close();
    }

    public static boolean esNumeroPerfecto(int num) {
        int sum = 0;
        for (int i = 1; i < num; i++) {
            if (num % i == 0) {
                sum += i;
            }
        }
        return sum == num;
    }
}
