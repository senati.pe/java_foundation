import java.util.Scanner;

public class algoritmo10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese los números separados por espacios: ");
        String numerosStr = sc.nextLine();
        String[] numerosArr = numerosStr.split(" ");
        int countPares = 0;
        for (int i = 0; i < numerosArr.length; i++) {
            int num = Integer.parseInt(numerosArr[i]);
            if (num % 2 == 0) {
                countPares++;
            }
        }
        System.out.println("Hay " + countPares + " números pares en la lista.");
        sc.close();
    }
}
