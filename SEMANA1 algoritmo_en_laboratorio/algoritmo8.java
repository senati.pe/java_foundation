import java.util.Scanner;

public class algoritmo8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese una fecha en formato dd/mm/yyyy: ");
        String fechaString = sc.nextLine();
        boolean fechaValida = true;
        try {
            String[] fechaPartes = fechaString.split("/");
            int dia = Integer.parseInt(fechaPartes[0]);
            int mes = Integer.parseInt(fechaPartes[1]);
            int anio = Integer.parseInt(fechaPartes[2]);
            if (mes < 1 || mes > 12) {
                fechaValida = false;
            } else if (dia < 1 || dia > diasEnMes(mes, anio)) {
                fechaValida = false;
            }
        } catch (Exception e) {
            fechaValida = false;
        }
        if (fechaValida) {
            System.out.println("La fecha es válida.");
        } else {
            System.out.println("La fecha no es válida.");
        }
        sc.close();
    }

    private static int diasEnMes(int mes, int anio) {
        int dias = 0;
        switch (mes) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                dias = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                dias = 30;
                break;
            case 2:
                if (esBisiesto(anio)) {
                    dias = 29;
                } else {
                    dias = 28;
                }
                break;
        }
        return dias;
    }

    private static boolean esBisiesto(int anio) {
        return ((anio % 4 == 0) && (anio % 100 != 0)) || (anio % 400 == 0);
    }
}
