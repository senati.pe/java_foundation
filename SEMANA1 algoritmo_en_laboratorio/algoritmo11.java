import java.util.Scanner;

public class algoritmo11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese los números separados por espacios: ");
        String numerosStr = sc.nextLine();
        String[] numerosArr = numerosStr.split(" ");
        int countMultiplosDe3 = 0;
        for (int i = 0; i < numerosArr.length; i++) {
            int num = Integer.parseInt(numerosArr[i]);
            if (num % 3 == 0) {
                countMultiplosDe3++;
            }
        }
        System.out.println("Hay " + countMultiplosDe3 + " múltiplos de 3 en la lista.");
        sc.close();
    }
}
