import java.util.Scanner;

public class algoritmo6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el peso de la persona en kilogramos: ");
        double peso = sc.nextDouble();
        System.out.print("Ingrese la estatura de la persona en metros: ");
        double estatura = sc.nextDouble();
        double imc = peso / (estatura * estatura);
        System.out.printf("El índice de masa corporal de la persona es %.2f.", imc);
        sc.close();
    }
}
