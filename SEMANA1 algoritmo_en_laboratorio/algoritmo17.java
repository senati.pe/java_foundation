import java.util.Scanner;

public class algoritmo17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese las palabras separadas por espacios: ");
        String palabrasStr = sc.nextLine();
        System.out.print("Ingrese la longitud mínima para contar las palabras: ");
        int longitudMinima = sc.nextInt();
        String[] palabrasArr = palabrasStr.split(" ");
        int countPalabrasConLongitud = 0;
        for (int i = 0; i < palabrasArr.length; i++) {
            String palabra = palabrasArr[i];
            if (palabra.length() > longitudMinima) {
                countPalabrasConLongitud++;
            }
        }
        System.out.println("Hay " + countPalabrasConLongitud + " palabras con longitud mayor a " + longitudMinima);
        sc.close();
    }
}
