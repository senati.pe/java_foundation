import java.util.Scanner;

public class algoritmo19 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese un número entero positivo: ");
        int num = sc.nextInt();
        double sum = 0;
        for (int i = 1; i <= num; i++) {
            sum += 1.0 / i;
        }
        if (sum == Math.round(sum)) {
            System.out.println(num + " es un número armónico");
        } else {
            System.out.println(num + " no es un número armónico");
        }
        sc.close();
    }
}
