import java.util.Scanner;

public class algoritmo18 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese las palabras separadas por espacios: ");
        String palabrasStr = sc.nextLine();
        System.out.print("Ingrese la longitud máxima para contar las palabras: ");
        int longitudMaxima = sc.nextInt();
        String[] palabrasArr = palabrasStr.split(" ");
        int countPalabrasConLongitud = 0;
        for (int i = 0; i < palabrasArr.length; i++) {
            String palabra = palabrasArr[i];
            if (palabra.length() < longitudMaxima) {
                countPalabrasConLongitud++;
            }
        }
        System.out.println("Hay " + countPalabrasConLongitud + " palabras con longitud menor a " + longitudMaxima);
        sc.close();
    }
}
