import java.util.Scanner;

public class algoritmo16 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese las palabras separadas por espacios: ");
        String palabrasStr = sc.nextLine();
        System.out.print("Ingrese la letra a buscar: ");
        char letra = sc.next().charAt(0);
        String[] palabrasArr = palabrasStr.split(" ");
        int countPalabrasConLetra = 0;
        for (int i = 0; i < palabrasArr.length; i++) {
            String palabra = palabrasArr[i];
            if (palabra.indexOf(letra) != -1) {
                countPalabrasConLetra++;
            }
        }
        System.out.println("Hay " + countPalabrasConLetra + " palabras que contienen la letra " + letra);
        sc.close();
    }
}
