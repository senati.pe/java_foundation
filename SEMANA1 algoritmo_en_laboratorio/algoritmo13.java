import java.util.Scanner;

public class algoritmo13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese los números separados por espacios: ");
        String numerosStr = sc.nextLine();
        String[] numerosArr = numerosStr.split(" ");
        int countNumerosFibonacci = 0;
        for (int i = 0; i < numerosArr.length; i++) {
            int num = Integer.parseInt(numerosArr[i]);
            if (esNumeroFibonacci(num)) {
                countNumerosFibonacci++;
            }
        }
        System.out.println("Hay " + countNumerosFibonacci + " números de Fibonacci en la lista.");
        sc.close();
    }

    public static boolean esNumeroFibonacci(int num) {
        int a = 0;
        int b = 1;
        while (b < num) {
            int c = a + b;
            a = b;
            b = c;
        }
        return b == num;
    }
}
