import java.util.Scanner;

public class algoritmo20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el número de elementos de la lista: ");
        int n = sc.nextInt();
        int[] numeros = new int[n];
        System.out.println("Ingrese los " + n + " números enteros de la lista:");
        for (int i = 0; i < n; i++) {
            numeros[i] = sc.nextInt();
        }
        System.out.print("Ingrese el número mínimo de divisores: ");
        int minDivisores = sc.nextInt();
        int cantidad = 0;
        for (int i = 0; i < n; i++) {
            int num = numeros[i];
            int contador = 0;
            for (int j = 1; j <= num; j++) {
                if (num % j == 0) {
                    contador++;
                }
            }
            if (contador > minDivisores) {
                cantidad++;
            }
        }
        System.out.println("Hay " + cantidad + " números en la lista con más de " + minDivisores + " divisores.");
        sc.close();
    }
}
