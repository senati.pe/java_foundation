import java.util.Scanner;

public class algoritmo4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el primer número entero: ");
        int lado1 = sc.nextInt();
        System.out.print("Ingrese el segundo número entero: ");
        int lado2 = sc.nextInt();
        System.out.print("Ingrese el tercer número entero: ");
        int lado3 = sc.nextInt();
        if (lado1 + lado2 > lado3 && lado1 + lado3 > lado2 && lado2 + lado3 > lado1) {
            System.out.println("Estos tres números pueden formar un triángulo válido.");
        } else {
            System.out.println("Estos tres números no pueden formar un triángulo válido.");
        }
        sc.close();
    }
}
