import java.text.DateFormat;
import java.util.Date;

public class ejercicio12 {
    public static void main(String[] args) {
        Date fechaActual = new Date();
        DateFormat formatoFecha = DateFormat.getDateTimeInstance(DateFormat.SHORT, DateFormat.MEDIUM);
        String fechaFormateada = formatoFecha.format(fechaActual);
        System.out.println("La fecha actual es: " + fechaFormateada);
    }
}
