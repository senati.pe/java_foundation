import java.math.BigInteger;

public class ejercicio15 {
    public static void main(String[] args) {
        BigInteger num1 = new BigInteger("123456789123456789");
        BigInteger num2 = new BigInteger("987654321987654321");
        BigInteger suma = num1.add(num2);
        BigInteger resta = num2.subtract(num1);
        BigInteger multiplicacion = num1.multiply(num2);
        BigInteger division = num2.divide(num1);
        System.out.println("La suma de " + num1 + " y " + num2 + " es " + suma);
        System.out.println("La resta de " + num2 + " y " + num1 + " es " + resta);
        System.out.println("La multiplicación de " + num1 + " y " + num2 + " es " + multiplicacion);
        System.out.println("La división de " + num2 + " entre " + num1 + " es " + division);
    }
}
