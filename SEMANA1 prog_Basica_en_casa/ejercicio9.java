public class ejercicio9 {
    public static void main(String[] args) {
        int numeroAbsoluto = Math.abs(-10);
        double raizCuadrada = Math.sqrt(25);
        int maximo = Math.max(10, 20);
        int minimo = Math.min(5, 15);
        double pi = Math.PI;
        System.out.println("El valor absoluto de -10 es: " + numeroAbsoluto);
        System.out.println("La raíz cuadrada de 25 es: " + raizCuadrada);
        System.out.println("El valor máximo entre 10 y 20 es: " + maximo);
        System.out.println("El valor mínimo entre 5 y 15 es: " + minimo);
        System.out.println("El valor de la constante PI es: " + pi);
    }
}
