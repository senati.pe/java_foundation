import java.util.Locale;

public class ejercicio17 {
    public static void main(String[] args) {
        Locale configuracionRegional1 = new Locale("es", "ES");
        Locale configuracionRegional2 = new Locale("en", "US");
        System.out.println("La configuración regional de España es " + configuracionRegional1.getDisplayName());
        System.out.println("La configuración regional de Estados Unidos es " + configuracionRegional2.getDisplayName());
    }
}
