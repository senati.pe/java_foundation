import java.util.Random;

public class ejercicio8 {
    public static void main(String[] args) {
        Random random = new Random();
        
        int numeroEntero = random.nextInt(100);
        
        double numeroDecimal = random.nextDouble();
        
        System.out.println("El número entero aleatorio generado es: " + numeroEntero);
        System.out.println("El número decimal aleatorio generado es: " + numeroDecimal);
    }
}
