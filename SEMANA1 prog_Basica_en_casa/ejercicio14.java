import java.math.BigDecimal;

public class ejercicio14 {
    public static void main(String[] args) {
        BigDecimal num1 = new BigDecimal("123456789.123456789");
        BigDecimal num2 = new BigDecimal("987654321.987654321");
        BigDecimal suma = num1.add(num2);
        BigDecimal resta = num2.subtract(num1);
        BigDecimal multiplicacion = num1.multiply(num2);
        BigDecimal division = num2.divide(num1, 10, BigDecimal.ROUND_HALF_UP);
        System.out.println("La suma de " + num1 + " y " + num2 + " es " + suma);
        System.out.println("La resta de " + num2 + " y " + num1 + " es " + resta);
        System.out.println("La multiplicación de " + num1 + " y " + num2 + " es " + multiplicacion);
        System.out.println("La división de " + num2 + " entre " + num1 + " es " + division);
    }
}
