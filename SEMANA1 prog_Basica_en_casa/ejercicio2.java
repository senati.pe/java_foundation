import java.util.HashMap;

public class ejercicio2 {
    public static void main(String[] args) {
        HashMap<String, Integer> mapa = new HashMap<String, Integer>();
        
        mapa.put("Juan", 30);
        mapa.put("Maria", 25);
        mapa.put("Pedro", 35);
        mapa.put("Ana", 28);
        
        for (String nombre : mapa.keySet()) {
            System.out.println(nombre + " tiene " + mapa.get(nombre) + " años.");
        }
    }
}
