import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        System.out.print("Ingresa una línea de texto: ");
        String lineaTexto = scanner.nextLine();
        
        System.out.println("La línea de texto ingresada es: " + lineaTexto);
        
        System.out.print("Ingresa un número: ");
        int numero = scanner.nextInt();
        
        System.out.println("El número ingresado es: " + numero);
        
        scanner.close();
    }
}
