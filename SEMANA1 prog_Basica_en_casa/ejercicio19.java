import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class ejercicio19 {
    public static void main(String[] args) {
        try {
            OutputStream salida = new FileOutputStream("salida.txt");
            String mensaje = "Este es un mensaje de prueba.";
            byte[] datos = mensaje.getBytes();
            salida.write(datos);
            salida.close();
            System.out.println("Se ha escrito el mensaje en el archivo.");
        } catch (IOException e) {
            System.out.println("Ha ocurrido un error al escribir en el archivo.");
            e.printStackTrace();
        }
    }
}
