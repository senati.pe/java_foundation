import java.util.regex.Pattern;

public class ejercicio10 {
    public static void main(String[] args) {
        Pattern patronEmail = Pattern.compile("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}");
        String email = "ejemplo@correo.com";
        boolean esEmail = patronEmail.matcher(email).matches();
        if (esEmail) {
            System.out.println(email + " es una dirección de correo electrónico válida.");
        } else {
            System.out.println(email + " no es una dirección de correo electrónico válida.");
        }
    }
}
