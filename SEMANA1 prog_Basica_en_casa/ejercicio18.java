import java.io.InputStream;
import java.util.Scanner;

public class ejercicio18 {
    public static void main(String[] args) {
        InputStream entrada = System.in;
        Scanner scanner = new Scanner(entrada);
        System.out.print("Ingresa tu nombre: ");
        String nombre = scanner.nextLine();
        System.out.println("¡Hola " + nombre + "!");
    }
}
