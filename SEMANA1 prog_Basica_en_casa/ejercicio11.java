import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ejercicio11 {
    public static void main(String[] args) {
        Pattern patronEmail = Pattern.compile("[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,}");
        String cadena = "Esta es una cadena que contiene una dirección de correo electrónico: ejemplo@correo.com";
        Matcher matcher = patronEmail.matcher(cadena);
        if (matcher.find()) {
            String emailEncontrado = matcher.group();
            System.out.println("Se encontró la dirección de correo electrónico: " + emailEncontrado);
        } else {
            System.out.println("No se encontró ninguna dirección de correo electrónico en la cadena.");
        }
    }
}
