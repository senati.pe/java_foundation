import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class ejercicio7 {
    public static void main(String[] args) {
        BufferedReader lector = null;
        try {
            lector = new BufferedReader(new FileReader("/ruta/del/archivo.txt"));
            
            String linea;
            while ((linea = lector.readLine()) != null) {
                System.out.println(linea);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                
                if (lector != null) lector.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
