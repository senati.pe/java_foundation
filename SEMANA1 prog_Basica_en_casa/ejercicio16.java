import java.util.TimeZone;

public class ejercicio16 {
    public static void main(String[] args) {
        TimeZone zonaHoraria1 = TimeZone.getTimeZone("America/Los_Angeles");
        TimeZone zonaHoraria2 = TimeZone.getTimeZone("Europe/Madrid");
        System.out.println("La zona horaria de Los Ángeles es " + zonaHoraria1.getDisplayName());
        System.out.println("La zona horaria de Madrid es " + zonaHoraria2.getDisplayName());
    }
}
