import java.util.Scanner;

public class ejercicio5 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        int option;
        do {
            System.out.println("Seleccione una opción:");
            System.out.println("1. Calcular área y perímetro de cuadrado");
            System.out.println("2. Calcular área y perímetro de rombo");
            System.out.println("3. Calcular área y perímetro de rectángulo");
            System.out.println("4. Calcular área y perímetro de circunferencia");
            System.out.println("0. Salir");
            option = input.nextInt();

            // calculate and draw the selected shape
            switch (option) {
                case 1:
                    square();
                    break;
                case 2:
                    rhombus();
                    break;
                case 3:
                    rectangle();
                    break;
                case 4:
                    circle();
                    break;
                case 0:
                    System.out.println("Saliendo del programa...");
                    break;
                default:
                    System.out.println("Opción inválida. Inténtelo de nuevo.");
                    break;
            }
        } while (option != 0);

        input.close();
    }

    public static void drawShape(int width, int height, char symbol) {
        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                if (row == 0 || row == height - 1 || col == 0 || col == width - 1) {
                    System.out.print(symbol);
                } else {
                    System.out.print(" ");
                }
            }
            System.out.println();
        }
    }

    public static void square() {
        Scanner input = new Scanner(System.in);

    System.out.println("Ingrese el lado del cuadrado:");
        double side = input.nextDouble();

        double area = Math.pow(side, 2);
        double perimeter = 4 * side;

        System.out.println("Área del cuadrado: " + area);
        System.out.println("Perímetro del cuadrado: " + perimeter);

        int width = (int) side;
        int height = (int) side;
        char symbol = '*';
        drawShape(width,
