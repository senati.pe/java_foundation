import java.util.Arrays;
import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);
        int[] miArreglo = new int[10];
        
        for (int i = 0; i < 10; i++) {
            System.out.print("Ingrese el número en la posición " + (i+1) + ": ");
            miArreglo[i] = entrada.nextInt();
        }
        
        bubbleSort(miArreglo, miArreglo.length);
        
        System.out.println("El arreglo ordenado de manera descendente es: " + Arrays.toString(miArreglo));
    }
    
    
    static void bubbleSort(int arr[], int n) {
        if (n == 1)
            return;
        
        for (int i = 0; i < n-1; i++) {
            if (arr[i] < arr[i+1]) {
                int temp = arr[i];
                arr[i] = arr[i+1];
                arr[i+1] = temp;
            }
        }
        
        bubbleSort(arr, n-1);
    }
}
