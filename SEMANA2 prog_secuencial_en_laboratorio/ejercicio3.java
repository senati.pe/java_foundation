import java.util.Scanner;

public class ejercicio3 {
    
    public static void main(String[] args) {
        String[][] estudiantes = new String[3][2];
        
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < estudiantes.length; i++) {
            System.out.println("Ingrese el nombre del estudiante " + (i+1) + ":");
            estudiantes[i][0] = scanner.nextLine();
            System.out.println("Ingrese la edad del estudiante " + (i+1) + ":");
            estudiantes[i][1] = scanner.nextLine();
        }
        
          int sumaEdades = 0;
            for (int i = 0; i < estudiantes.length; i++) {
            sumaEdades += Integer.parseInt(estudiantes[i][1]);
        }
        double promedioEdades = (double)sumaEdades / estudiantes.length;
        
        System.out.println("Datos de los estudiantes:");
            for (int i = 0; i < estudiantes.length; i++) {
            System.out.println("Estudiante " + (i+1) + ": " + estudiantes[i][0] + ", " + estudiantes[i][1] + " años");
        }
        System.out.println("La edad promedio de los estudiantes es: " + promedioEdades + " años");
    }
}
