public class ejercicio4 {
    public static void main(String[] args) {
        int[] arr = {1, 5, 3, 2, 4};
        int maximo = encontrarMaximo(arr);
        System.out.println("El maximo elemento del arreglo es: " + maximo);
    }

    public static int encontrarMaximo(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
