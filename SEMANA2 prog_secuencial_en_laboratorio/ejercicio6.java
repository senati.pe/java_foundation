import java.math.BigInteger;
import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("¿Cuántos números ingresará?: ");
        int n = scanner.nextInt();
        BigInteger[] factorials = new BigInteger[n];

        for (int i = 0; i < n; i++) {
            System.out.print("Ingrese un número: ");
            int number = scanner.nextInt();
            BigInteger factorial = BigInteger.ONE;
             for (int j = 1; j <= number; j++) {
                factorial = factorial.multiply(BigInteger.valueOf(j));
            }
            factorials[i] = factorial;
        }

        for (int i = 0; i < n; i++) {
            System.out.println("El factorial de " + (i + 1) + " es: " + factorials[i]);
        }
    }
}
