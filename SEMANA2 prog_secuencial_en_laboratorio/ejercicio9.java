public class ejercicio9 {

    public static void main(String[] args) {

        int[] arreglo = {2, 4, 6, 8, 10};

        double varianza = calcularVarianza(arreglo);

    System.out.println("La varianza del arreglo es: " + varianza);
    }

    public static double calcularVarianza(int[] arreglo) {
        double suma = 0;
        for (int i = 0; i < arreglo.length; i++) {
            suma += arreglo[i];
        }
         double media = suma / arreglo.length;

        double sumaDiferencias = 0;
        for (int i = 0; i < arreglo.length; i++) {
            double diferencia = arreglo[i] - media;
            sumaDiferencias += diferencia * diferencia;
        }

         double varianza = sumaDiferencias / arreglo.length;
        return varianza;
    }
}
