public class ejercicio7 {

    public static void main(String[] args) {
        
        int[] arreglo = {5, 10, 15, 20, 25};
        
        int posicion = buscarElemento(arreglo, 15);
        
        if (posicion != -1) {
            System.out.println("El elemento 15 se encuentra en la posición " + posicion);
        } else {
             System.out.println("El elemento 15 no se encuentra en el arreglo");
        }
    }

    public static int buscarElemento(int[] arreglo, int elemento) {
        for (int i = 0; i < arreglo.length; i++) {
            if (arreglo[i] == elemento) {
                return i;
            }
        }
        return -1;
    }
}
