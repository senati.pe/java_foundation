public class ejercicio2{
public static int multiplicarEnteros(int[] arreglo) {
    int producto = 1;
    for (int i = 0; i < arreglo.length; i++) {
        producto *= arreglo[i];
    }
    return producto;
}
int[] arreglo = {1, 2, 3, 4, 5};
int resultado = multiplicarEnteros(arreglo);
System.out.println("el producto de los elementos del arreglo es: " + resultado);
}