import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero;
        String[] simbolos = {"M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"};
        int[] valores = {1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1};
        
        do {
            System.out.print("Ingresa un número entero positivo: ");
            numero = sc.nextInt();
        } while(numero <= 0);
        
         String numeroRomano = "";
        int i = 0;
        
        while(numero > 0) {
            int cociente = numero / valores[i];
            
            for(int j = 0; j < cociente; j++) {
                numeroRomano += simbolos[i];
                numero -= valores[i];
            }
            
            i++;
        }
        
        System.out.printf("El número %d en números romanos es: %s", numero, numeroRomano);
        
        sc.close();
    }
}
