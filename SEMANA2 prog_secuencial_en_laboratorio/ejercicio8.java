import java.util.Arrays;
import java.util.HashSet;

public class ejercicio8 {
   public static void main(String[] args) {
      int[] inputArray = { 1, 2, 3, 3, 4, 5, 5, 6 };
      HashSet&lt;Integer&gt; set = new HashSet&lt;Integer&gt;();

       for (int i = 0; i &lt; inputArray.length; i++) {
         set.add(inputArray[i]);
      }

      Object[] uniqueArray = set.toArray();
      System.out.println("Array with duplicates: " + Arrays.toString(inputArray));
      System.out.println("Array without duplicates: " + Arrays.toString(uniqueArray));
   }
}
