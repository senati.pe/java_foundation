import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int opcion;
        boolean valido = false;
        String mensaje = "";
        
        do {
            System.out.print("Ingresa un número del 1 al 7: ");
            opcion = sc.nextInt();
            
            switch(opcion) {
                case 1:
                    mensaje = "Hoy aprenderemos sobre programación";
                    valido = true;
                    break;
                case 2:
                    mensaje = "¿Qué tal tomar un curso de marketing digital?";
                    valido = true;
                    break;
                case 3:
                    mensaje = "Hoy es un gran día para comenzar a aprender de diseño";
                    valido = true;
                    break;
                case 4:
                    mensaje = "¿Y si aprendemos algo de negocios online?";
                    valido = true;
                    break;
                case 5:
                    mensaje = "Veamos un par de clases sobre producción audiovisual";
                    valido = true;
                    break;
                case 6:
                    mensaje = "Tal vez sea bueno desarrollar una habilidad blanda";
                    valido = true;
                    break;
                case 7:
                    mensaje = "Yo decido distraerme programando";
                    valido = true;
                    break;
                default:
                    System.out.println("El número ingresado no es válido. Intente de nuevo.");
                    break;
            }
        } while(!valido);
        
        System.out.println(mensaje);
        sc.close();
    }
}
