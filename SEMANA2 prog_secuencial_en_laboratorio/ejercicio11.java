import java.util.Scanner;

public class ejercicio11 {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
             System.out.print("Ingrese el tamaño del arreglo: ");
        int tamano = scanner.nextInt();

        int[] numeros = new int[tamano];
        for (int i = 0; i < numeros.length; i++) {
            numeros[i] = (int) (Math.random() * 300) + 1;
        }

         System.out.print("Ingrese el dígito a buscar: ");
        int digito = scanner.nextInt();
        while (digito < 0 || digito > 9) {
            System.out.print("El dígito debe estar entre 0 y 9, por favor ingrese un valor válido: ");
            digito = scanner.nextInt();
        }

        int[] numerosTerminadosEnDigito = new int[numeros.length];
         int cantidadNumerosTerminadosEnDigito = 0;
        for (int i = 0; i < numeros.length; i++) {
            int ultimoDigito = numeros[i] % 10;
            if (ultimoDigito == digito) {
                numerosTerminadosEnDigito[cantidadNumerosTerminadosEnDigito] = numeros[i];
                cantidadNumerosTerminadosEnDigito++;
            }
        }

        System.out.println("Los números que terminan en " + digito + " son:");
        for (int i = 0; i < cantidadNumerosTerminadosEnDigito; i++) {
            System.out.println(numerosTerminadosEnDigito[i]);
        }
    }
}
