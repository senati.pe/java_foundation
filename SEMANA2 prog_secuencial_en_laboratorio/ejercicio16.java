import java.util.Scanner;

public class ejercicio16{

let students = [];

while (true) {
    let name = prompt("Enter student name (or * to exit):");
    if (name === "*") {
        break;
    }
    let age = parseInt(prompt("Enter student age:"));
    students.push({ name, age });
}

let legalAgeStudents = students.filter(student => student.age >= 18);

let maxAge = Math.max(...students.map(student => student.age));
let oldestStudents = students.filter(student => student.age === maxAge);

console.log("Legal age students:");
legalAgeStudents.forEach(student => console.log(student.name));

console.log("Oldest students:");
oldestStudents.forEach(student => {
    let lang = "";
    if (student.age % 2 === 0) {
        lang = "Java";
    } else {
        lang = "Español";
    }
    console.log(`${student.name} (${lang})`);
});
}