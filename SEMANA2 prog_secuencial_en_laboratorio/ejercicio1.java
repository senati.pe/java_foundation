public class ejercicio1{
public static int sumarEnteros(int[] arreglo) {
    int suma = 0;
    for (int i = 0; i < arreglo.length; i++) {
        suma += arreglo[i];
    }
    return suma;
}
int[] arreglo = {1, 2, 3, 4, 5};
int resultado = sumarEnteros(arreglo);
System.out.println("La suma de los elementos del arreglo es: " + resultado);
}