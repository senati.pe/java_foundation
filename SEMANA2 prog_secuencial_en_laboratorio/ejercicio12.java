import java.util.Scanner;

public class ejercicio12 {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] respuestas = new String[2];
        
            System.out.print("¿Está lloviendo? (sí/no): ");
        respuestas[0] = sc.nextLine().toLowerCase();
        
        if (respuestas[0].equals("sí")) {
            System.out.print("¿Está haciendo mucho viento? (sí/no): ");
            respuestas[1] = sc.nextLine().toLowerCase();
            
            if (respuestas[1].equals("sí")) {
                System.out.println("Hase mucho viento para salir con una sombrilla.");
            } else {
                System.out.println("Lleva una sombrilla por si acaso.");
            }
        } else {
            System.out.println("Que tengas un bonito día.");
        }
        
        sc.close();
    }

}
