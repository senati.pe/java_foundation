import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numero;
        
        do {
               System.out.print("Ingresa un número entre 1000 y 7000: ");
            numero = sc.nextInt();
        } while(numero < 1000 || numero > 7000);
        
        int[] digitos = new int[4];
        int i = 0;
        
        while(numero > 0) {
            digitos[i] = numero % 10;
            numero /= 10;
            i++;
        }
        
        for(int j = 3; j >= 0; j--) {
            System.out.printf("[%d] = %d\n", j, digitos[j]);
        }
        
        sc.close();
    }
}
