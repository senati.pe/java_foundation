public class ejercicio12 {
    public static void main(String[] args) {
        int[] arr = {10, 5, 20, 15, 30};
        int max = findMax(arr);
        System.out.println("El numero mas grande es: " + max);
    }

    public static int findMax(int[] arr) {
        int max = arr[0];
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
            }
        }
        return max;
    }
}
