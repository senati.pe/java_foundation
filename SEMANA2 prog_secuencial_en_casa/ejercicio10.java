import java.util.*;

public class ejercicio10 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 2, 3, 4, 4, 4, 5, 5, 5, 5}; 
        int mode = findMode(arr); 
        System.out.println("La moda es: " + mode); 
    }

    public static int findMode(int[] arr) {
        Map<Integer, Integer> freqMap = new HashMap<>(); 
        int mode = 0; 
        int maxFreq = 0; 
        for (int num : arr) {
            int freq = freqMap.getOrDefault(num, 0) + 1; 
            freqMap.put(num, freq); 
            if (freq > maxFreq) { 
                mode = num; 
                maxFreq = freq; 
            }
        }
        return mode; 
    }
}
