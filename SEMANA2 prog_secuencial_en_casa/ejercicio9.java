public class ejercicio9 {
    public static void main(String[] args) {
        int[] numeros = {5, 2, 8, 1, 9};

        for(int i = 0; i < numeros.length - 1; i++) {
            for(int j = 0; j < numeros.length - i - 1; j++) {
                if(numeros[j] > numeros[j+1]) {
                    int temp = numeros[j];
                    numeros[j] = numeros[j+1];
                    numeros[j+1] = temp;
                }
            }
        }

        double mediana;
        int tamano = numeros.length;
        if (tamano % 2 == 0) {
            int index
