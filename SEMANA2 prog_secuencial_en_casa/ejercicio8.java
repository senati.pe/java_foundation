import java.util.*;

public class ejercicio8 {
    public static void main(String[] args) {
        int[] arr = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29}; 
        int count = countPairsWithPrimeSum(arr); 
           System.out.println("El número de parejas con suma prima es: " + count); 
    }

    public static int countPairsWithPrimeSum(int[] arr) {
        int count = 0; 
        for (int i = 0; i < arr.length; i++) {
            for (int j = i + 1; j < arr.length; j++) {
                int sum = arr[i] + arr[j]; 
                if (isPrime(sum)) { 
                    count++;
                }
            }
        }
        return count; 
    }

    public static boolean isPrime(int num) {
        if (num <= 1) { 
            return false;
        }
        for (int i = 2; i <= Math.sqrt(num); i++) { 
            if (num % i == 0) {
                return false; 
            }
        }
        return true; 
    }
}
