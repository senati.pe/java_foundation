import java.util.Arrays;

public class ejercicio5 {
    public static void main(String[] args) {
        int[] nums = {5, 6, 3, 5, 7, 8, 9, 1, 2};

        int[] increasingSubArray = findLongestIncreasingSubArray(nums);

        System.out.println(Arrays.toString(increasingSubArray));
    }

    public static int[] findLongestIncreasingSubArray(int[] nums) {
        int endIndex = 0;
        int maxLength = 0;

        for (int i = 0; i < nums.length; i++) {
            int j = i;
            int length = 1;

            while (j + 1 < nums.length && nums[j + 1] > nums[j]) {
                j++;
                length++;
            }

            if (length > maxLength) {
                maxLength = length;
                endIndex = j;
            }
        }

        int[] subArray = Arrays.copyOfRange(nums, endIndex - maxLength + 1, endIndex + 1);

        return subArray;
    }
}
