import org.apache.commons.math3.linear.*;

public class ejercicio4 {
  public static void main(String[] args) {
    double[][] data = {{1, 0, 0, 0, 0, 0, 0},
                       {0, 2, 0, 0, 0, 0, 0},
                       {0, 0, 3, 0, 0, 0, 0},
                       {0, 0, 0, 4, 0, 0, 0},
                       {0, 0, 0, 0, 5, 0, 0},
                       {0, 0, 0, 0, 0, 6, 0},
                       {0, 0, 0, 0, 0, 0, 7}};
    RealMatrix matrix = MatrixUtils.createRealMatrix(data);

    EigenDecomposition eigDecomp = new EigenDecomposition(matrix);
    RealMatrix jordan = eigDecomp.getV().multiply(eigDecomp.getD()).multiply(MatrixUtils.inverse(eigDecomp.getV()));
    System.out.println("Matriz de Jordan:");
    System.out.println(jordan);
  }
}
