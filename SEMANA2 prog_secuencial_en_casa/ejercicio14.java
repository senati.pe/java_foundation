import java.util.Arrays;

public class ejercicio14 {
    public static void main(String[] args) {
        int[] arr = {5, 7, 8, 8, 8, 10, 12};
        int num = 8;
        int count = countOccurrences(arr, num);
        System.out.println("El número " + num + " aparece " + count + " veces en el arreglo.");
    }

    public static int countOccurrences(int[] arr, int num) {
        Arrays.sort(arr); // Ordenar el arreglo
        int firstIndex = findFirstIndex(arr, num); 
        int lastIndex = findLastIndex(arr, num); 
              if (firstIndex == -1 || lastIndex == -1) { 
            return 0;
        }
        return lastIndex - firstIndex + 1; 
    }

    public static int findFirstIndex(int[] arr, int num) {
        int low = 0;
        int high = arr.length - 1;
        int result = -1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (arr[mid] == num) {
                result = mid;
                high = mid - 1;
            } else if (arr[mid] > num) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return result;
    }

    public static int findLastIndex(int[] arr, int num) {
        int low = 0;
        int high = arr.length - 1;
        int result = -1;
        while (low <= high) {
            int mid = (low + high) / 2;
            if (arr[mid] == num) {
                result = mid;
                low = mid + 1;
            } else if (arr[mid] > num) {
                high = mid - 1;
            } else {
                low = mid + 1;
            }
        }
        return result;
    }
}
