public class ejercicio18 {
    public static void main(String[] args) {
        int[] arr1 = {1, 2, 3};
        int[] arr2 = {4, 5, 6};
        int dotProduct = calculateDotProduct(arr1, arr2);
        System.out.println("El producto punto es: " + dotProduct);
    }

    public static int calculateDotProduct(int[] arr1, int[] arr2) {
        int dotProduct = 0;
        for (int i = 0; i < arr1.length; i++) {
            dotProduct += arr1[i] * arr2[i];
        }
        return dotProduct;
    }
}
