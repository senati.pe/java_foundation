import java.util.HashMap;
import java.util.Map;

public class ejercicio16 {
    public static void main(String[] args) {
        int[] arr = {1, 2, 3, 4, 5, 2, 2, 4, 5, 5, 5};
        int mostFrequentNum = findMostFrequentNum(arr);
              System.out.println("El número que se repite más veces es: " + mostFrequentNum);
    }

    public static int findMostFrequentNum(int[] arr) {
        Map<Integer, Integer> frequencyMap = new HashMap<>();
        int maxFrequency = 0;
        int mostFrequentNum = -1;
              for (int num : arr) {
            int frequency = frequencyMap.getOrDefault(num, 0) + 1;
            frequencyMap.put(num, frequency);
            if (frequency > maxFrequency) {
                maxFrequency = frequency;
                mostFrequentNum = num;
            }
        }
        return mostFrequentNum;
    }
}
