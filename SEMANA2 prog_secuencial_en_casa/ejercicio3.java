public class ejercicio3 {
    public static void main(String[] args) {
      int[][] matrix = new int[7][7];
      
      for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 7; j++) {
          matrix[i][j] = (int)(Math.random() * 10);
        }
      }
      
      System.out.println("Matriz original:");
      for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 7; j++) {
          System.out.print(matrix[i][j] + " ");
        }
        System.out.println();
      }
      
      int[][] rotatedMatrix = new int[7][7];
      for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 7; j++) {
          rotatedMatrix[i][j] = matrix[6-j][i];
        }
      }
      
      System.out.println("Matriz rotada:");
      for (int i = 0; i < 7; i++) {
        for (int j = 0; j < 7; j++) {
          System.out.print(rotatedMatrix[i][j] + " ");
        }
        System.out.println();
      }
    }
  }
  