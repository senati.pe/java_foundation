public class ejercicio17 {
    public static void main(String[] args) {
       int[] arr = {1, 2, 3, 4, 5, 6, 7, 8, 9};
       int[] arr1 = new int[arr.length / 2];
       int[] arr2 = new int[arr.length - arr1.length];
         System.arraycopy(arr, 0, arr1, 0, arr1.length);

         System.arraycopy(arr, arr1.length, arr2, 0, arr2.length);

         System.out.println(Arrays.toString(arr1));

         System.out.println(Arrays.toString(arr2));
    }
 }
 