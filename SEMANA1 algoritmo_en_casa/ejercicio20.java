public class ejercicio20{
public static int countNarcissisticNumbers(int start, int end) {
    int count = 0;
    for (int num = start; num <= end; num++) {
        int sum = 0;
        int temp = num;
        int numDigits = String.valueOf(num).length();
        while (temp > 0) {
            int digit = temp % 10;
            sum += Math.pow(digit, numDigits);
            temp /= 10;
        }
        if (sum == num) {
            count++;
        }
    }
    return count;
}
}