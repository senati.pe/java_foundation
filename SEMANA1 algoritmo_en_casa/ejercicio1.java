import java.util.Arrays;

public class ejercicio1 {

    public static int[] encontrarParSuma(int num, int[] arr) {
        int[] resultado = new int[2];
        for (int i = 0; i < arr.length; i++) {
            int complemento = num - arr[i];
            if (Arrays.binarySearch(arr, complemento) >= 0) {
                resultado[0] = arr[i];
                resultado[1] = complemento;
                return resultado;
            }
        }
        return null;
    }

    public static void main(String[] args) {
        int[] numeros = {1, 2, 3, 4, 5};
        int numDeterminado = 7;
        int[] resultado = encontrarParSuma(numDeterminado, numeros);
        if (resultado != null) {
            System.out.println("El par de números cuya suma es " + numDeterminado + " es: " + resultado[0] + " y " + resultado[1]);
        } else {
            System.out.println("No se encontró ningún par de números cuya suma sea igual a " + numDeterminado);
        }
    }
}
