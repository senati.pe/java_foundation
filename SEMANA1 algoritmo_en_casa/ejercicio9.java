public class ejercicio9 {

    public static int contarPalabras(String[] palabras) {
        int contador = 0;
        for (String palabra : palabras) {
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                int apariciones = 0;
                for (int j = 0; j < palabra.length(); j++) {
                    if (palabra.charAt(j) == letra) {
                        apariciones++;
                    }
                }
                if (apariciones % 2 != 0) {
                    contador++;
                    break;
                }
            }
        }
        return contador;
    }

    public static void main(String[] args) {
        String[] palabras = {"hello", "world", "programming", "java", "odd", "numbers"};
        int palabrasConLetraRepetidaImpar = contarPalabras(palabras);
        System.out.println("Hay " + palabrasConLetraRepetidaImpar + " palabras con una letra repetida un número impar de veces.");
    }
}
