public class ejercicio16 {
    public static void main(String[] args) {
        int limite = 1000;

        int contador = 0;
        for (int i = 1; i <= limite; i++) {
            if (esNumeroArmstrong(i)) {
                contador++;
            }
        }

        System.out.println("El número de números de Armstrong hasta " + limite + " es: " + contador);
    }

    public static boolean esNumeroArmstrong(int numero) {
        int suma = 0;
        int temp = numero;
        int numDigitos = cuentaDigitos(numero);

        while (temp != 0) {
            int digito = temp % 10;
            suma += Math.pow(digito, numDigitos);
            temp /= 10;
        }

        return suma == numero;
    }

    public static int cuentaDigitos(int numero) {
        int cuenta = 0;
        while (numero != 0) {
            cuenta++;
            numero /= 10;
        }
        return cuenta;
    }
}
