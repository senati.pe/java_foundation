public class ejercicio11 {

    public static int contarPalabras(List<String> palabras) {
        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.length() >= 4 && palabra.charAt(0) == palabra.charAt(3)) {
                contador++;
            }
        }
        return contador;
    }

    public static void main(String[] args) {
        List<String> palabras = Arrays.asList("hola", "mesa", "luna", "casa", "carro", "popo");
        int palabrasConLetraEnPosiciones = contarPalabras(palabras);
        System.out.println("Hay " + palabrasConLetraEnPosiciones + " palabras que tienen la misma letra en las posiciones 1 y 4.");
    }
}
