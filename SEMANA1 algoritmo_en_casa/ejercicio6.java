public class ejercicio6 {

    public static boolean esDivisible(int numero) {
        for (int i = 1; i <= 20; i++) {
            if (numero % i != 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int numero = 20;
        while (numero <= 1000000) {
            if (esDivisible(numero)) {
                System.out.println("El número " + numero + " es divisible por todos los números de 1 a 20.");
                break;
            }
            numero += 20;
        }
        if (numero > 1000000) {
            System.out.println("No se encontró ningún número divisible por todos los números de 1 a 20.");
        }
    }
}
