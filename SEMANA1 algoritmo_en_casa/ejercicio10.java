public class ejercicio10 {

    public static int factorial(int n) {
        if (n == 0 || n == 1) {
            return 1;
        } else {
            return n * factorial(n - 1);
        }
    }

    public static boolean esIgualASumaFactoriales(int n) {
        int sumaFactoriales = 0;
        int numero = n;
        while (numero > 0) {
            int digito = numero % 10;
            sumaFactoriales += factorial(digito);
            numero /= 10;
        }
        return (sumaFactoriales == n);
    }

    public static int contarNumeros(int maximo) {
        int contador = 0;
        for (int i = 1; i <= maximo; i++) {
            if (esIgualASumaFactoriales(i)) {
                contador++;
            }
        }
        return contador;
    }

    public static void main(String[] args) {
        int maximo = 10000;
        int numerosConPropiedad = contarNumeros(maximo);
        System.out.println("Hay " + numerosConPropiedad + " números entre 1 y " + maximo + " que tienen la propiedad de ser iguales a la suma de los dígitos factoriales de su descomposición en dígitos.");
    }
}
