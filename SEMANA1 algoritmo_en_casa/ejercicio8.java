public class ejercicio8 {

    public static int sumaDigitos(int numero) {
        int suma = 0;
        while (numero > 0) {
            suma += numero % 10;
            numero /= 10;
        }
        return suma;
    }

    public static boolean esDivisible(int numero) {
        int suma = 0;
        for (int i = 1; i <= 100; i++) {
            suma += sumaDigitos(i);
        }
        return numero % suma == 0;
    }

    public static void main(String[] args) {
        int numero = 12345;
        if (esDivisible(numero)) {
            System.out.println(numero + " es divisible por la suma de los dígitos de los números de 1 a 100.");
        } else {
            System.out.println(numero + " no es divisible por la suma de los dígitos de los números de 1 a 100.");
        }
    }
}
