import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.print("Ingresa una oración: ");
        String oracion = input.nextLine().toLowerCase();

        int contadorPalabras = 0;

        String[] palabras = oracion.split(" ");
        for (String palabra : palabras) {
            boolean hayRepetido = false;
            for (int i = 0; i < palabra.length(); i++) {
                char letra = palabra.charAt(i);
                int contador = 0;
                for (int j = 0; j < palabra.length(); j++) {
                    if (palabra.charAt(j) == letra) {
                        contador++;
                    }
                }
                if (contador > 2 && contador < 5) {
                    hayRepetido = true;
                    break;
                }
            }
            if (hayRepetido) {
                contadorPalabras++;
            }
        }

        System.out.println("Número de palabras con una letra que aparece más de dos veces pero menos de cinco veces: " + contadorPalabras);
    }
}
