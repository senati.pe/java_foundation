public class ejercicio18{
public static boolean isHarshad(int n) {
    int sum = 0;
    int num = n;
    while (num > 0) {
        int digit = num % 10;
        sum += digit;
        num /= 10;
    }
    return n % sum == 0;
}

public static int countHarshad(int start, int end) {
    int count = 0;
    for (int i = start; i <= end; i++) {
        if (isHarshad(i)) {
            count++;
        }
    }
    return count;
}
}