public class ejercicio12 {

    public static boolean esPrimo(int numero) {
        if (numero <= 1) {
            return false;
        }
        for (int i = 2; i <= Math.sqrt(numero); i++) {
            if (numero % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static List<Integer> factorizar(int numero) {
        List<Integer> factoresPrimos = new ArrayList<Integer>();
        for (int i = 2; i <= numero; i++) {
            if (esPrimo(i)) {
                while (numero % i == 0) {
                    factoresPrimos.add(i);
                    numero /= i;
                }
            }
        }
        return factoresPrimos;
    }

    public static int sumaDigitos(int numero) {
        int suma = 0;
        while (numero > 0) {
            int digito = numero % 10;
            if (digito != 0) {
                suma += digito;
            }
            numero /= 10;
        }
        return suma;
    }

    public static int sumaDigitosPrimos(List<Integer> factoresPrimos) {
        int suma = 0;
        for (int factorPrimo : factoresPrimos) {
            if (factorPrimo < 10) {
                suma += factorPrimo;
            } else {
                suma += sumaDigitos(factorPrimo);
            }
        }
        return suma;
    }

    public static boolean esNumeroDeSmith(int numero) {
        if (esPrimo(numero)) {
            return false;
        }
        int sumaDigitosNumero = sumaDigitos(numero);
        List<Integer> factoresPrimos = factorizar(numero);
        int sumaDigitosPrimos = sumaDigitosPrimos(factoresPrimos);
        return sumaDigitosPrimos == sumaDigitosNumero;
    }

    public static int contarNumerosDeSmith(int numeroMaximo) {
        int contador = 0;
        for (int i = 1; i <= numeroMaximo; i++) {
            if (esNumeroDeSmith(i)) {
                contador++;
            }
        }
        return contador;
    }

    public static void main(String[] args) {
        int numeroMaximo = 1000;
        int numerosDeSmith = contarNumerosDeSmith(numeroMaximo);
        System.out.println("Hay " + numerosDeSmith + " números de Smith hasta " + numeroMaximo);
    }
}
