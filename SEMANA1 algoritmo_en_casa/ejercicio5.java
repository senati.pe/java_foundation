public class ejercicio5 {

    public static boolean contieneVocales(String palabra) {
        return palabra.toLowerCase().contains("a") &&
                palabra.toLowerCase().contains("e") &&
                palabra.toLowerCase().contains("i") &&
                palabra.toLowerCase().contains("o") &&
                palabra.toLowerCase().contains("u");
    }

    public static void main(String[] args) {
        String[] palabras = {"aeiou", "hola", "mundo", "murcielago", "amor"};
        int contadorConVocales = 0;
        for (String palabra : palabras) {
            if (contieneVocales(palabra)) {
                contadorConVocales++;
            }
        }
        System.out.println("Hay " + contadorConVocales + " palabras que contienen todas las vocales.");
    }
}
