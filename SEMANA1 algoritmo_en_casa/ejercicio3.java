public class ejercicio3 {

    public static boolean esNumeroPerfecto(int num) {
        int sumaDivisores = 0;
        for (int i = 1; i <= num / 2; i++) {
            if (num % i == 0) {
                sumaDivisores += i;
            }
        }
        return sumaDivisores == num;
    }

    public static boolean esNumeroFibonacci(int num) {
        int a = 0, b = 1, c = 0;
        while (c < num) {
            c = a + b;
            a = b;
            b = c;
        }
        return c == num;
    }

    public static void main(String[] args) {
        int inicio = 1, fin = 100;
        int contadorPerfectos = 0, contadorFibonacci = 0;
        for (int i = inicio; i <= fin; i++) {
            if (esNumeroPerfecto(i)) {
                contadorPerfectos++;
            }
            if (esNumeroFibonacci(i)) {
                contadorFibonacci++;
            }
        }
        System.out.println("En el rango [" + inicio + ", " + fin + "]:");
        System.out.println("- Hay " + contadorPerfectos + " números perfectos.");
        System.out.println("- Hay " + contadorFibonacci + " números de Fibonacci.");
    }
}
