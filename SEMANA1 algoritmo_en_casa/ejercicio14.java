public class ejercicio14 {
    public static void main(String[] args) {
        int limiteInferior = 1;
        int limiteSuperior = 10000;
        int contadorKaprekar = 0;

        for (int i = limiteInferior; i <= limiteSuperior; i++) {
            int cuadrado = i * i;
            String cuadradoStr = String.valueOf(cuadrado);
            for (int j = 1; j < cuadradoStr.length(); j++) {
                String parteIzquierda = cuadradoStr.substring(0, j);
                String parteDerecha = cuadradoStr.substring(j);
                int sumaPartes = Integer.parseInt(parteIzquierda) + Integer.parseInt(parteDerecha);
                if (sumaPartes == i) {
                    contadorKaprekar++;
                    break;
                }
            }
        }

        System.out.println("Número de números de Kaprekar encontrados: " + contadorKaprekar);
    }
}
