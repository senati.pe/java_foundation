public class ejercicio17 {
    public static void main(String[] args) {
        String[] palabras = {"hola", "adios", "manzana", "murcielago", "cascabel"};
        int contador = 0;

        for (String palabra : palabras) {
            if (tieneLetraRepetidaDosVeces(palabra)) {
                contador++;
            }
        }

        System.out.println("El número de palabras con una letra repetida exactamente dos veces es: " + contador);
    }

    public static boolean tieneLetraRepetidaDosVeces(String palabra) {
        char[] letras = palabra.toCharArray();
        int[] contadorLetras = new int[26];

        for (char letra : letras) {
            int indice = letra - 'a';
            contadorLetras[indice]++;
        }

        for (int contador : contadorLetras) {
            if (contador == 2) {
                return true;
            }
        }

        return false;
    }
}
