public class ejercicio15 {
    public static void main(String[] args) {
        String[] palabras = {"anana", "pera", "manzana", "reconocer", "hola", "ama"};

        int contador = 0;
        for (String palabra : palabras) {
            if (palabra.charAt(0) == palabra.charAt(palabra.length() - 1)) {
                contador++;
            }
        }

        System.out.println("El número de palabras con la misma letra al principio y al final es: " + contador);
    }
}
