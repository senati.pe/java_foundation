public class ejercicio7 {

    public static int contarPalabras(String[] palabras) {
        int contador = 0;
        for (String palabra : palabras) {
            char[] letras = palabra.toCharArray();
            for (int i = 0; i < letras.length - 2; i++) {
                if (letras[i] == letras[i + 1] && letras[i] == letras[i + 2]) {
                    contador++;
                    break;
                }
            }
        }
        return contador;
    }

    public static void main(String[] args) {
        String[] palabras = {"abbbb", "ccc", "def", "ghhijklll"};
        int contador = contarPalabras(palabras);
        System.out.println("Hay " + contador + " palabras que tienen la misma letra repetida tres veces consecutivas.");
    }
}
