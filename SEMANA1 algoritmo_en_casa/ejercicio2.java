public class ejercicio2 {

    public static int contarPalabrasConsecutivas(String texto) {
        int contador = 0;
        String[] palabras = texto.split(" ");
        for (String palabra : palabras) {
            int numVocalesConsecutivas = 0;
            boolean tieneVocalesConsecutivas = false;
            for (char letra : palabra.toCharArray()) {
                if ("aeiouAEIOU".indexOf(letra) != -1) {
                    numVocalesConsecutivas++;
                    if (numVocalesConsecutivas > 2) {
                        tieneVocalesConsecutivas = true;
                    }
                } else {
                    numVocalesConsecutivas = 0;
                }
            }
            if (tieneVocalesConsecutivas) {
                contador++;
            }
        }
        return contador;
    }

    public static void main(String[] args) {
        String texto = "Este es un ejemplo de texto con varias palabras que tienen vocales consecutivas como aura, auto, buenísimo, diario, etc.";
        int resultado = contarPalabrasConsecutivas(texto);
        System.out.println(resultado); // Devuelve 3 porque "aura", "buenísimo" y "diario" tienen más de dos vocales consecutivas.
    }
}
