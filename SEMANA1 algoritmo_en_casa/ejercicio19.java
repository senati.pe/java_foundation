import java.util.HashSet;
import java.util.List;
import java.util.Set;
public class ejercicio19{
public static int countWordsWithAllDistinctLetters(List<String> words) {
    int count = 0;
    for (String word : words) {
        Set<Character> charSet = new HashSet<>();
        for (char c : word.toCharArray()) {
            charSet.add(c);
        }
        if (charSet.size() == word.length()) {
            count++;
        }
    }
    return count;
}
}