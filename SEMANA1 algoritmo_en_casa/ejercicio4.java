public class ejercicio4 {

    public static boolean esDivisiblePorTodos(int num) {
        for (int i = 1; i <= 10; i++) {
            if (num % i != 0) {
                return false;
            }
        }
        return true;
    }

    public static void main(String[] args) {
        int inicio = 1, fin = 100;
        int contadorDivisibles = 0;
        for (int i = inicio; i <= fin; i++) {
            if (esDivisiblePorTodos(i)) {
                contadorDivisibles++;
            }
        }
        System.out.println("En el rango [" + inicio + ", " + fin + "]:");
        System.out.println("- Hay " + contadorDivisibles + " números divisibles por todos los números de 1 a 10.");
    }
}
