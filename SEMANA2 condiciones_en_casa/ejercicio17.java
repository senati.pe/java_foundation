import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese el tamaño del vector: ");
        int tam = sc.nextInt();
        int[] vector = new int[tam];
        System.out.println("Ingrese los valores del vector: ");
        for (int i = 0; i < tam; i++) {
            vector[i] = sc.nextInt();
        }
        int max = vector[0];
        int min = vector[0];
        for (int i = 1; i < tam; i++) {
            if (vector[i] > max) {
                max = vector[i];
            }
            if (vector[i] < min) {
                min = vector[i];
            }
        }
        System.out.println("El valor máximo del vector es " + max);
        System.out.println("El valor mínimo del vector es " + min);
    }
}
