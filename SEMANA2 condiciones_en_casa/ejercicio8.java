import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Introduce un número: ");
        int num = sc.nextInt();
        int sum = 0;
        int temp = num;
        while(temp > 0) {
            int digit = temp % 10;
            sum += digit;
            temp /= 10;
        }
        if(num % sum == 0) {
            System.out.println(num + " es un número de Harshad");
        } else {
            System.out.println(num + " no es un número de Harshad");
        }
    }
}
