import java.util.Scanner;

public class ejercicio14 {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce un número entero positivo: ");
        int num = entrada.nextInt();
        
        System.out.print("Los números impares desde 1 hasta " + num + " son: ");
        for (int i = 1; i <= num; i++) {
            if (i % 2 != 0) {
                System.out.print(i + "; ");
            }
        }
    }
}
