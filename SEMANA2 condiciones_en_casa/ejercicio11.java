public class ejercicio11 {
    public static void main(String[] args) {
        double montoInicial = 10.0;
        double montoTotalPagado = montoInicial;
        double montoMensual = montoInicial;
        
        for (int i = 2; i <= 20; i++) {
            montoMensual *= 2;
            montoTotalPagado += montoMensual;
        }
        
        double montoMensualFinal = montoTotalPagado / 20;
        
        System.out.println("El monto mensual a pagar es: S/" + montoMensualFinal);
        System.out.println("El monto total pagado después de 20 meses es: S/" + montoTotalPagado);
    }
}
