import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese la cantidad de números que desea ordenar: ");
        int n = sc.nextInt();
        int[] numeros = new int[n];

        System.out.println("Ingrese los números: ");
        for (int i = 0; i < n; i++) {
            numeros[i] = sc.nextInt();
        }

        for (int i = 1; i < n; i++) {
            int actual = numeros[i];
            int j = i - 1;
            while (j >= 0 && numeros[j] > actual) {
                numeros[j + 1] = numeros[j];
                j--;
            }
            numeros[j + 1] = actual;
        }

        System.out.println("Números ordenados: ");
        for (int numero : numeros) {
            System.out.print(numero + " ");
        }
    }
}
