public class ejercicio15 {

    public static void main(String[] args) {
        
        double saldo = 5000;
        double tasaInteres = 0.016;
        int tiempoPago = 18;
        
        for (int mes = 1; mes <= tiempoPago; mes++) {
            double interes = saldo * tasaInteres;
            saldo += interes;
            System.out.println("Mes " + mes + ": saldo = " + saldo);
        }
    }
}
