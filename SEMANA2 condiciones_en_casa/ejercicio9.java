import java.util.Scanner;

public class ejercicio9 {
    public static void main(String[] args) {

        Scanner entrada = new Scanner(System.in);

        System.out.print("Ingrese el valor de n: ");
        int n = entrada.nextInt();

        System.out.print("Ingrese el valor de a: ");
        double a = entrada.nextDouble();

        System.out.print("Ingrese el valor de r: ");
        double r = entrada.nextDouble();

        double suma = 0;

        for (int i = 0; i < n; i++) {
            suma += a * Math.pow(r, i);
        }

        System.out.println("La suma de los primeros " + n + " términos de la serie geométrica es: " + suma);

    }
}
