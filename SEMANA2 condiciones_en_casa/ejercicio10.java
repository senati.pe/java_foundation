import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;

public class ejercicio10 {
    
    public static void main(String[] args) {
        int[] datos = leerArchivo("datos.txt");
        
        double promedio = calcularPromedio(datos);
        System.out.println("Promedio: " + promedio);
        
        int moda = calcularModa(datos);
        System.out.println("Moda: " + moda);
        
        double desviacion = calcularDesviacion(datos, promedio);
        System.out.println("Desviación estándar: " + desviacion);
    }
    
    public static int[] leerArchivo(String archivo) {
        int[] datos = null;
        try {
            BufferedReader br = new BufferedReader(new FileReader(archivo));
            String linea = br.readLine();
            String[] valores = linea.split(" ");
            datos = new int[valores.length];
            for (int i = 0; i < valores.length; i++) {
                datos[i] = Integer.parseInt(valores[i]);
            }
            br.close();
        } catch (IOException e) {
            System.out.println("Error al leer el archivo");
        }
        return datos;
    }
    
    public static double calcularPromedio(int[] datos) {
        int suma = 0;
        for (int i = 0; i < datos.length; i++) {
            suma += datos[i];
        }
        return (double) suma / datos.length;
    }
    
    public static int calcularModa(int[] datos) {
        Arrays.sort(datos);
        int moda = datos[0];
        int maxFrecuencia = 1;
        int frecuenciaActual = 1;
        for (int i = 1; i < datos.length; i++) {
            if (datos[i] == datos[i-1]) {
                frecuenciaActual++;
            } else {
                if (frecuenciaActual > maxFrecuencia) {
                    moda = datos[i-1];
                    maxFrecuencia = frecuenciaActual;
                }
                frecuenciaActual = 1;
            }
        }
        if (frecuenciaActual > maxFrecuencia) {
            moda = datos[datos.length-1];
        }
        return moda;
    }
    
    public static double calcularDesviacion(int[] datos, double promedio) {
        double suma = 0;
        for (int i = 0; i < datos.length; i++) {
            suma += Math.pow(datos[i] - promedio, 2);
        }
        return Math.sqrt(suma / datos.length);
    }
}
