import java.util.Scanner;

public class ejercicio7 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Ingrese el número de términos a calcular: ");
        int n = input.nextInt();
        int[] sucesion = new int[n];
        sucesion[0] = 1;
        sucesion[1] = 2;
        for (int i = 2; i < n; i++) {
            int siguiente = sucesion[i-1] + 1;
            boolean esSuma = true;
            while (esSuma) {
                esSuma = false;
                for (int j = 0; j < i-1; j++) {
                    for (int k = j+1; k < i; k++) {
                        if (sucesion[j] + sucesion[k] == siguiente) {
                            esSuma = true;
                            siguiente++;
                            break;
                        }
                    }
                    if (esSuma) {
                        break;
                    }
                }
            }
            sucesion[i] = siguiente;
        }
        System.out.println("Los primeros " + n + " términos de la sucesión de Ulam son:");
        for (int i = 0; i < n; i++) {
            System.out.print(sucesion[i] + " ");
        }
        System.out.println();
    }
}
