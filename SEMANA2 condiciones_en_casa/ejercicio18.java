import java.util.Scanner;

public class ejercicio18 {
    
    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        int limiteInf, limiteSup, num, suma = 0, fueraIntervalo = 0, igualLimites = 0;
        
        do {
            System.out.print("Introduce el límite inferior del intervalo: ");
            limiteInf = entrada.nextInt();
            System.out.print("Introduce el límite superior del intervalo: ");
            limiteSup = entrada.nextInt();
            if (limiteInf > limiteSup) {
                System.out.println("El límite inferior debe ser menor o igual que el límite superior. Vuelve a introducir los límites del intervalo.");
            }
        } while (limiteInf > limiteSup);
        
        do {
            System.out.print("Introduce un número (0 para terminar): ");
            num = entrada.nextInt();
            if (num == limiteInf || num == limiteSup) {
                igualLimites++;
            }
            if (num > limiteSup || num < limiteInf) {
                fueraIntervalo++;
            } else {
                suma += num;
            }
        } while (num != 0);
        
        System.out.println("La suma de los números dentro del intervalo (" + limiteInf + ", " + limiteSup + ") es: " + suma);
        System.out.println("Hay " + fueraIntervalo + " números fuera del intervalo (" + limiteInf + ", " + limiteSup + ").");
        if (igualLimites > 0) {
            System.out.println("Se han introducido " + igualLimites + " números iguales a los límites del intervalo.");
        }
        
        entrada.close();
    }

}
