import java.io.IOException;

public class ejercicio13 {

    public static void main(String[] args) throws InterruptedException, IOException {
        System.out.print("Ingresa la cantidad de segundos: ");
        int segundos = Integer.parseInt(System.console().readLine());
        System.out.println("Presione enter para iniciar...");
        System.in.read();
        
        for (int i = segundos; i >= 0; i--) {
            System.out.println(i);
            Thread.sleep(1000);
        }
    }
}
