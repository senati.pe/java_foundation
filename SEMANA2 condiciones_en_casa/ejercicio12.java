import java.util.Scanner;

public class ejercicio12 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad de números primos que desea mostrar: ");
        int n = sc.nextInt(); 
        int i = 2; 
        int contador = 0; 
        while (contador < n) {
            boolean esPrimo = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) { 
                    esPrimo = false;
                    break;
                }
            }
            if (esPrimo) {
                System.out.println(i); 
                contador++; 
            }
            i++; 
        }
    }
}
