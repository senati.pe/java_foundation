import java.util.Scanner;

public class ejercicio16 {

    public static void main(String[] args) {
        
        Scanner entrada = new Scanner(System.in);
        System.out.print("Introduce un número entero: ");
        int num = entrada.nextInt();
        
        for (int i = 1; i <= num; i++) {
            for (int j = (2 * i) - 1; j >= 1; j -= 2) {
                System.out.print(j + " ");
            }
            System.out.println();
        }
    }
}
