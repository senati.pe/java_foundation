import java.util.Scanner;

public class Persona {
    public static void main(String[] args) {
        Persona persona = new Persona();
        System.out.println(persona.toString());
    }
    
    private String nombre;
    private int edad;
    private String DNI;
    private char sexo;
    private double peso;
    private double altura;

    public Persona() {
        pedirDatos();
    }

    public Persona(String nombre, int edad, char sexo, double peso, double altura) {
        this.nombre = nombre;
        this.edad = edad;
        this.sexo = sexo;
        this.peso = peso;
        this.altura = altura;
        generarDNI();
    }

    private boolean esHombre() {
        return sexo == 'H';
    }

    public int calcularIMC() {
        double imc = peso / Math.pow(altura, 2);

        if (imc < 20) {
            return -1;
        } else if (imc >= 20 && imc <= 25) {
            return 0;
        } else {
            return 1;
        }
    }

    public boolean esMayorDeEdad() {
        return edad >= 18;
    }

    public void generarDNI() {
        int numDNI = (int) (Math.random() * (100000000 - 10000000) + 10000000);
        char letraDNI = "TRWAGMYFPDXBNJZSQVHLCKE".charAt(numDNI % 23);
        DNI = Integer.toString(numDNI) + letraDNI;
    }

    public void pedirDatos() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Ingrese el nombre: ");
        nombre = scanner.nextLine();

        System.out.print("Ingrese la edad: ");
        edad = scanner.nextInt();

        System.out.print("Ingrese el sexo (H/M): ");
        sexo = scanner.next().charAt(0);

        System.out.print("Ingrese el peso (en kg): ");
        peso = scanner.nextDouble();

        System.out.print("Ingrese la altura (en m): ");
        altura = scanner.nextDouble();

        generarDNI();
    }

    public String getNombre() {
        return nombre;}
    public int getEdad() {
          return edad; }
    public String getDNI() {
        return DNI;  }
    public char getSexo() {
         return sexo; }
    public double getPeso() {
        return peso;  }
    public double getAltura() {
        return altura;  }
    public void setNombre(String nombre) {
        this.nombre = nombre;   }
    public void setEdad(int edad) {
        this.edad = edad;}
    public void setDNI(String DNI) {
        this.DNI = DNI;   }
    public void setSexo(char sexo) {
        this.sexo = sexo;  }
    public void setPeso(double peso) {
        this.peso = peso; }
    public void setAltura(double altura) {
        this.altura = altura;  }
    @Override
    public String toString() {
        return "Persona{" +
                "nombre='" + nombre + '\'' +
                ", edad=" + edad +
                ", DNI='" + DNI + '\'' +
                ", sexo=" + sexo +
                ", peso=" + peso +
                ", altura=" + altura +
                '}';
    }
}
