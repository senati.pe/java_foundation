import java.util.Scanner;

public class CuentaBancaria {
    private String titular;
    private double cantidad;

    public CuentaBancaria(String titular, double cantidad) {
        this.titular = titular;
        if (cantidad < 0) {
            this.cantidad = 0;
        } else {
            this.cantidad = cantidad;
        }
    }

    public String getTitular() {
        return titular;}
    public void setTitular(String titular) {
        this.titular = titular;  }
    public double getCantidad() {
        return cantidad;   }
    public void setCantidad(double cantidad) {
        this.cantidad = cantidad;   }
    public void ingresar(double cantidad) {
        if (cantidad > 0) {
            this.cantidad += cantidad;
        }
    }
    public void retirar(double cantidad) {
        if (this.cantidad - cantidad < 0) {
            this.cantidad = 0;
        } else {
            this.cantidad -= cantidad;
        }
    }
    public String toString() {
        return "Titular: " + titular + " | Cantidad: " + cantidad;
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el nombre del titular de la cuenta: ");
        String titular = sc.nextLine();
        System.out.print("Ingrese la cantidad de la cuenta: ");
        double cantidad = sc.nextDouble();
        Cuenta cuenta = new Cuenta(titular, cantidad);
        System.out.println("Cuenta creada: " + cuenta.toString());
        System.out.print("Ingrese la cantidad a ingresar en la cuenta: ");
        double ingreso = sc.nextDouble();
        cuenta.ingresar(ingreso);
        System.out.println("Cuenta actualizada: " + cuenta.toString());
        System.out.print("Ingrese la cantidad a retirar de la cuenta: ");
        double retiro = sc.nextDouble();
        cuenta.retirar(retiro);
        System.out.println("Cuenta actualizada: " + cuenta.toString());
    }
}
