public class Automovil {
    private String marca;
    private int modelo;
    private double motor;
    private String tipoCombustible;
    private String tipoAutomovil;
    private int numPuertas;
    private int cantAsientos;
    private int velocidadMaxima;

    public Automovil(String marca, int modelo, double motor, String tipoCombustible, String tipoAutomovil, int numPuertas, int cantAsientos, int velocidadMaxima) {
        this.marca = marca;
        this.modelo = modelo;
        this.motor = motor;
        this.tipoCombustible = tipoCombustible;
        this.tipoAutomovil = tipoAutomovil;
        this.numPuertas = numPuertas;
        this.cantAsientos = cantAsientos;
        this.velocidadMaxima = velocidadMaxima;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public int getModelo() {
        return modelo;
    }

    public void setModelo(int modelo) {
        this.modelo = modelo;
    }

    public double getMotor() {
        return motor;
    }

    public void setMotor(double motor) {
        this.motor = motor;
    }

    public String getTipoCombustible() {
        return tipoCombustible;
    }

    public void setTipoCombustible(String tipoCombustible) {
        this.tipoCombustible = tipoCombustible;
    }

    public String getTipoAutomovil() {
        return tipoAutomovil;
    }

    public void setTipoAutomovil(String tipoAutomovil) {
        this.tipoAutomovil = tipoAutomovil;
    }

    public int getNumPuertas() {
        return numPuertas;
    }

    public void setNumPuertas(int numPuertas) {
        this.numPuertas = numPuertas;
    }

    public int getCantAsientos() {
        return cantAsientos;
    }

    public void setCantAsientos(int cantAsientos) {
        this.cantAsientos = cantAsientos;
    }

    public int getVelocidadMaxima() {
        return velocidadMaxima;
    }

    public void setVelocidadMaxima(int velocidadMaxima) {
        this.velocidadMaxima = velocidadMaxima;
    }

    public static void main(String[] args) {
        Automovil miAuto = new Automovil("Toyota", 2021, 1.6, "gasolina", "compacto", 4, 5, 200);
        System.out.println("Mi auto es un " + miAuto.getTipoAutomovil() + " de marca " + miAuto.getMarca() + " del año " + miAuto.getModelo() + " con motor de " + miAuto.getMotor() + " litros y funciona con " + miAuto.getTipoCombustible() + ".");
        System.out.println("Tiene " + miAuto.getNumPuertas() + " puertas y " + miAuto.getCantAsientos() + " asientos, y su velocidad máxima es de " + miAuto.getVelocidadMaxima() + " km/h.");
    }
}
