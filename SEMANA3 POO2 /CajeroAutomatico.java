import java.util.Scanner;

public class CajeroAutomatico {
    private static String[] clientesDNI = {"12345678", "23456789", "34567890", "45678901", "56789012"};
    private static String[] clientesPassword = {"password1", "password2", "password3", "password4", "password5"};
    private static double[] clientesSaldo = {1000, 2000, 1500, 3000, 2500};
    
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        boolean sesionActiva = false;
        int intentos = 3;
        int clienteIndex = -1;
        
        while (!sesionActiva && intentos > 0) {
            System.out.print("Ingrese su DNI: ");
            String dni = input.nextLine();
            System.out.print("Ingrese su contraseña: ");
            String password = input.nextLine();
            
            for (int i = 0; i < clientesDNI.length; i++) {
                if (dni.equals(clientesDNI[i]) && password.equals(clientesPassword[i])) {
                    clienteIndex = i;
                    sesionActiva = true;
                    break;
                }
            }
            
            if (!sesionActiva) {
                intentos--;
                System.out.println("DNI o contraseña incorrectos. Quedan " + intentos + " intentos.");
            }
        }
        
        if (sesionActiva) {
            System.out.println("Bienvenido " + clientesDNI[clienteIndex]);
            int opcion = 0;
            while (opcion != 4) {
                System.out.println("Elija la operación que desea realizar:");
                System.out.println("[1] Depósito");
                System.out.println("[2] Retiro");
                System.out.println("[3] Consulta de saldo");
                System.out.println("[4] Salir");
                opcion = input.nextInt();
                
                switch (opcion) {
                    case 1:
                        System.out.print("Ingrese la cantidad a depositar: ");
                        double deposito = input.nextDouble();
                        clientesSaldo[clienteIndex] += deposito;
                        System.out.println("Depósito exitoso. Nuevo saldo: " + clientesSaldo[clienteIndex]);
                        break;
                    case 2:
                        System.out.print("Ingrese la cantidad a retirar: ");
                        double retiro = input.nextDouble();
                        if (retiro > clientesSaldo[clienteIndex]) {
                            System.out.println("Saldo insuficiente.");
                        } else {
                            clientesSaldo[clienteIndex] -= retiro;
                            System.out.println("Retiro exitoso. Nuevo saldo: " + clientesSaldo[clienteIndex]);
                        }
                        break;
                    case 3:
                        System.out.println("Su saldo actual es: " + clientesSaldo[clienteIndex]);
                        break;
                    case 4:
                        System.out.println("Gracias por usar el cajero automático.");
                        break;
                    default:
                        System.out.println("Opción inválida. Por favor elija una opción válida.");
                        break;
                }
            }
        } else {
            System.out.println("Ha superado el número máximo de intentos. La sesión se cerrará.");
        }
    }
}
