import java.util.Scanner;

public class ejercicio10 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce el grado del polinomio (máximo 7): ");
        int grado = sc.nextInt();

        if (grado > 7) {
            System.out.println("El grado del polinomio no puede ser mayor que 7.");
            return;
        }

        double[] coeficientes = new double[grado + 1];

        for (int i = grado; i >= 0; i--) {
            System.out.print("Introduce el coeficiente de grado " + i + ": ");
            coeficientes[i] = sc.nextDouble();
        }

        String polinomio = "";
        for (int i = grado; i >= 0; i--) {
            if (coeficientes[i] != 0) {
                if (i == 0) {
                    polinomio += coeficientes[i];
                } else if (i == 1) {
                    polinomio += coeficientes[i] + "X + ";
                } else {
                    polinomio += coeficientes[i] + "X^" + i + " + ";
                }
            }
        }

        System.out.println("El polinomio introducido es: " + polinomio);
    }
}
