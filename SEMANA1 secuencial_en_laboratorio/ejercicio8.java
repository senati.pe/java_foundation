import java.util.Scanner;

public class ejercicio8 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce el lado 1 del trapecio: ");
        double lado1 = sc.nextDouble();

        System.out.print("Introduce el lado 2 del trapecio: ");
        double lado2 = sc.nextDouble();

        System.out.print("Introduce la altura del trapecio: ");
        double altura = sc.nextDouble();

        double area = ((lado1 + lado2) / 2) * altura;

        System.out.println("El área del trapecio es: " + area);
    }
}
