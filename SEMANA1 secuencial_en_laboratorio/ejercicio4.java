import java.util.Scanner;

public class ejercicio4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce la longitud del lado a: ");
        double a = sc.nextDouble();

        System.out.print("Introduce la longitud del lado b: ");
        double b = sc.nextDouble();

        System.out.print("Introduce la longitud del lado c: ");
        double c = sc.nextDouble();

        double s = (a + b + c) / 2;
        double area = Math.sqrt(s * (s - a) * (s - b) * (s - c));

        System.out.println("El área del triángulo es: " + area);
    }
}
