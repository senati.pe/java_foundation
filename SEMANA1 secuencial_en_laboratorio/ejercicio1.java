import java.util.Scanner;

public class ejercicio1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce el valor del semieje mayor (a): ");
        double a = sc.nextDouble();

        System.out.print("Introduce el valor del semieje menor (b): ");
        double b = sc.nextDouble();

        double area = Math.PI * a * b;
        double perimetro = 2 * Math.PI * Math.sqrt((a * a + b * b) / 2);

        System.out.println("El área de la elipse es: " + area);
        System.out.println("El perímetro de la elipse es: " + perimetro);
    }
}
