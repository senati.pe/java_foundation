import java.util.Scanner;

public class ejercicio3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce la longitud del cateto a: ");
        double a = sc.nextDouble();

        System.out.print("Introduce la longitud del cateto b: ");
        double b = sc.nextDouble();

        double h = Math.sqrt(a*a + b*b);

        System.out.println("La longitud de la hipotenusa es: " + h);
    }
}
