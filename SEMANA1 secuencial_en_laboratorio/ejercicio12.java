public class ejercicio12 {
    public static void main(String[] args) {
        double presupuestoAnual = 1000000.0; 
        double presupuestoOncologia = presupuestoAnual * 0.55;
        double presupuestoPedriatria = presupuestoAnual * 0.2;
        double presupuestoTraumatologia = presupuestoAnual * 0.25;

        System.out.println("El presupuesto anual de Oncología es: S/ " + presupuestoOncologia);
        System.out.println("El presupuesto anual de Pediatría es: S/ " + presupuestoPedriatria);
        System.out.println("El presupuesto anual de Traumatología es: S/ " + presupuestoTraumatologia);
    }
}
