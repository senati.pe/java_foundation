import java.util.Scanner;

public class ejercicio6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce un número entero: ");
        int numero = sc.nextInt();

        double raiz = Math.sqrt(numero);

        if (raiz == (int)raiz) {
            System.out.println("La raíz cuadrada de " + numero + " es " + (int)raiz);
        } else {
            System.out.println("La raíz cuadrada de " + numero + " es " + raiz);
        }
    }
}
