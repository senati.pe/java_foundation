import java.util.Scanner;

public class ejercicio14 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el radio del cilindro: ");
        double radio = sc.nextDouble();
        System.out.print("Ingrese la altura del cilindro: ");
        double altura = sc.nextDouble();
        
        double superficieLateral = 2.0 * Math.PI * radio * altura;
        double volumen = Math.PI * Math.pow(radio, 2.0) * altura;
        
        System.out.println("La superficie lateral del cilindro es: " + superficieLateral);
        System.out.println("El volumen del cilindro es: " + volumen);
    }
}
