import java.util.Scanner;

public class ejercicio9 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce el primer lado del triángulo: ");
        double lado1 = sc.nextDouble();

        System.out.print("Introduce el segundo lado del triángulo: ");
        double lado2 = sc.nextDouble();

        System.out.print("Introduce el tercer lado del triángulo: ");
        double lado3 = sc.nextDouble();

        if ((lado1*lado1 + lado2*lado2 == lado3*lado3) || (lado1*lado1 + lado3*lado3 == lado2*lado2) || (lado2*lado2 + lado3*lado3 == lado1*lado1)) {
            System.out.println("Los lados ingresados forman un triángulo rectángulo.");
            double seno = lado1 / lado3;
            double coseno = lado2 / lado3;
            double tangente = lado1 / lado2;
            System.out.println("Seno: " + seno);
            System.out.println("Coseno: " + coseno);
            System.out.println("Tangente: " + tangente);
        } else {
            System.out.println("Los lados ingresados no forman un triángulo rectángulo.");
        }
    }
}
