import java.util.Scanner;

public class ejercicio18 {
   public static void main(String[] args) {
      Scanner sc = new Scanner(System.in);
      double precio_inicial, precio_final;

      System.out.print("Ingrese el monto total de la compra: S/ ");
      precio_inicial = sc.nextDouble();

      precio_final = precio_inicial - (precio_inicial * 0.15);

      System.out.println("El monto total con descuento es: S/ " + precio_final);
   }
}
