import java.util.Scanner;

public class ejercicio13 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la temperatura en grados Celsius: ");
        double celsius = sc.nextDouble();
        double fahrenheit = (9.0 / 5.0) * celsius + 32.0;
        System.out.println(celsius + " grados Celsius equivale a " + fahrenheit + " grados Fahrenheit.");
    }
}
