import java.util.Scanner;

public class ejercicio5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Introduce el número de lados del polígono regular: ");
        int n = sc.nextInt();

        System.out.print("Introduce la longitud de cada lado: ");
        double lado = sc.nextDouble();

        double perimetro = n * lado;

        System.out.println("El perímetro del polígono regular es: " + perimetro);
    }
}
