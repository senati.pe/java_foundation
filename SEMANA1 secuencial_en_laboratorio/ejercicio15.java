import java.util.Scanner;

public class ejercicio15 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese la cantidad a cambiar: ");
        int cantidad = sc.nextInt();
        
        int[] denominaciones = {200, 100, 50, 20, 10, 5, 2, 1};
        int[] cambio = new int[8];
        
        for(int i=0; i<8; i++) {
            cambio[i] = cantidad / denominaciones[i];
            cantidad %= denominaciones[i];
        }
        
        System.out.println("Se necesitan los siguientes billetes/monedas:");
        for(int i=0; i<8; i++) {
            if(cambio[i] > 0) {
                System.out.println(cambio[i] + " billete(s)/moneda(s) de " + denominaciones[i]);
            }
        }
    }
}
