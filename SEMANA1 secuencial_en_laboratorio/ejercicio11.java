import java.util.Scanner;

public class ejercicio11 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        System.out.print("Ingrese el nombre del vendedor: ");
        String nombre = sc.nextLine();

        System.out.print("Ingrese el número de equipos exclusivos vendidos: ");
        int equiposExclusivos = sc.nextInt();

        System.out.print("Ingrese el valor total facturado: ");
        double valorFacturado = sc.nextDouble();

        double comision = (equiposExclusivos * 50) + (valorFacturado * 0.07);
        double salarioTotal = 2500 + comision;

        System.out.println("El salario total del vendedor " + nombre + " es: S/ " + salarioTotal);
    }
}
