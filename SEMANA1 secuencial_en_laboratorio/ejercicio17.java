import java.util.Scanner;

public class ejercicio17 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Ingrese el tiempo de estacionamiento en horas y minutos: ");
        int horas = sc.nextInt();
        int minutos = sc.nextInt();
        double tiempo = horas + minutos / 60.0; 
        double monto = Math.ceil(tiempo) * 2.5; 
        System.out.println("El monto total a pagar es: S/ " + monto);
    }
}
